package view;
import java.awt.*;
import java.util.*;
import javax.swing.*;
import model.*;

/**
 * 
 * Ce panel repr�sente la liste des tweets qui ont �t� collect�s.
 * 
 */
public class TweetPanel extends JPanel implements Observer{
	
	/**
	 * Liste graphique des panels de tweets.
	 */
	private JList<TweetDisplayer> listplayer;
	
	/**
	 * Mod�le de la liste des tweets.
	 */
	private DefaultListModel<TweetDisplayer> displayers;
	
	/**
	 * Mod�le du progralle pour que l'on puisse r�cup�rer les tweets.
	 */
	private TweetManifold modele;
	
	/**
	 * Largeur du composant graphique.
	 */
	private final int width = 650;
	
	/**
	 * Hauteur du composant graphique.
	 */
	private final int height = 380;
	
	public TweetPanel (TweetManifold modele){
		super();
		this.modele = modele;
		
		displayers = new DefaultListModel<TweetDisplayer>();
		listplayer = new JList<TweetDisplayer>(displayers);
		
		//On d�finit l'affichage des composants de la JList
		listplayer.setCellRenderer(new TweetCellRenderer());
		
		//On cr�e la barre de d�filement et on d�finit sa taille
		JScrollPane pane = new JScrollPane(listplayer);
		pane.setMinimumSize(new Dimension(width, height));
		pane.setSize(new Dimension(width,  height));
		pane.setPreferredSize(new Dimension(width,  height));
		pane.setMaximumSize(new Dimension(width,  height));
		
		this.add(pane);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		//On r�cup�re la liste de tweets de la collection
		ArrayList<Tweet> tweets = this.modele.getTweets();	
		
		if(this.modele.getQuery().getQuery() == null || this.modele.getQuery().getQuery().equals("")){
			displayers.clear();
			for(Tweet t : tweets){
				displayers.addElement(new TweetDisplayer(t));
			}
		}else{
			if(tweets.size() == 0){
				//S'il n'y a pas de tweets, on affiche un message d'erreurs.
				JOptionPane.showMessageDialog(null,"Aucune entr�e correspondante !", "Erreur", JOptionPane.ERROR_MESSAGE);
			}else{
				Tweet last_tweet = tweets.get(tweets.size() - 1);
				displayers.addElement(new TweetDisplayer(last_tweet));
			}
		}
	}
	
	/**
	 * M�thode pour nettoyer la JList.
	 */
	public void clean(){
		displayers.clear();
	}
}

/**
 * 
 * Cette classe d�finit l'affichage de chaque tweet dans la JList.
 *
 */
class TweetCellRenderer extends JLabel implements ListCellRenderer{

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		
		TweetDisplayer td = (TweetDisplayer) value;
		
		//On ajoute une bordure pour s�parer les tweets
		td.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		return td;
	}
}
