package view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import org.apache.derby.database.Database;

import model.DatabaseManager;
import model.HashtagLooter;
import model.LanguageLooter;
import model.Looter;
import model.TooManyRequestsException;
import model.TweetManifold;
import model.ConnectionManager;
import model.UnknownLooterException;
import twitter4j.Status;
import twitter4j.TwitterException;

public class ConsoleViewTweet implements Observer {
	private ArrayList<Status> tweets;
	private TweetManifold modele;
	private Scanner console;
	private String search;
	private String s;
	private LanguageLooter lang;
	private HashtagLooter core;
	private int n;
	private boolean bool = true;
	private DatabaseManager db;
	
	
	public ConsoleViewTweet(TweetManifold modele) {
		this.modele=modele;
		this.console=new Scanner(System.in);
		ManageTweetLogin();
	}
	

	
	public void ManageTweetLogin(){
		try {
			db=new DatabaseManager();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		while (bool){
			n=0;
			s="";
			
			while(!(s.equals("y") || s.equals("n"))){
				System.out.println("Voulez vous faire une recherche. y/n");
				s=this.console.nextLine();
			}
			if(s.equals("y")){
				System.out.println("rentrer un crit�re de recherche");
				search=this.console.nextLine();
				
					this.modele.setQuery(search);
				Thread t=new Thread(this.modele);
					t.start();
					try {
						t.join();
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					/*try {
						this.modele.collect();
					} catch (TwitterException e1) {
						e1.printStackTrace();
					} catch (TooManyRequestsException e1) {
						e1.printStackTrace();
					}*/
				
				this.tweets=this.modele.getCollection(); 
				System.out.println("S�lection des tweet");

				if(!this.tweets.isEmpty()){
					
					for(int i=0;i<(this.tweets.size());i++){
						System.out.println("text du tweet : "+this.tweets.get(i).getText());
						System.out.println("auteur du tweet : "+this.tweets.get(i).getUser().getName());
						System.out.println("Date du tweet : "+this.tweets.get(i).getCreatedAt());
						System.out.println("\n");
					}
				}
				
				else{
					System.out.println("pas de tweet pour cette fois try again. 404 tweet not found ");
				}
				System.out.println("\n"+"R�partition des langues selon les tweets s'appliquant � la recherche pr�sent�e"+"\n");
				try {
					lang= (LanguageLooter) this.modele.getLooter(new LanguageLooter());
					HashMap<String, Double>	data=lang.toPercent();
					for(String l : data.keySet()){
						System.out.println(l+": "+ data.get(l));
					}
				} catch (UnknownLooterException e) {
					e.printStackTrace();
				}			
				System.out.println("\n"+"Relation entre les hashtags et popularit� des hashtags"+"\n");
				
				try {
					core= (HashtagLooter) this.modele.getLooter(new HashtagLooter());
					for(String l : core.data().keySet()){
						n=n+core.data().get(l);
						System.out.println(l+": "+core.data().get(l));
					}
				} catch (UnknownLooterException e) {
					e.printStackTrace();
				}
				s="";
				System.out.println("\n");
				while(!(s.equals("y") || s.equals("n"))){
					System.out.println("Voulez vous sauvegarder y/n");
					s=this.console.nextLine();
				}
				if (s.equals("y")){
					try {
						db.write(modele);
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("Votre base de donn�e a �t� sauvegard�e");
				}
			}
				s="";
				while(!(s.equals("y") || s.equals("n"))){
					System.out.println("voulez vous voir vos pr�c�dentes recherches? y/n");
					s=this.console.nextLine();
				}
				
				if(s.equalsIgnoreCase("y")){

					while(!(s.equals("t")|| s.equals("h") || s.equals("l"))){
						System.out.println("Pour afficher les tweets pr�c�dents taper t. Pour afficher les hashtags taper h.\n Pour afficher les langues taper l");
						s=this.console.nextLine();					
						if(s.equals("t")){
							try {
								db.tableaffiche("TWEET");
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
						if(s.equals("h")){
							try {
								db.tableaffiche("TWEETHASTAGS");
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
						if(s.equals("l")){
							try {
								db.tableaffiche("LANGUAGE");
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				}

				
				s="";
				while(!(s.equals("y") || s.equals("n"))){
					System.out.println("Voulez vous recommencer y/n");
					s=this.console.nextLine();
				}
				

				
				if (s.equals("n"))
					bool=false;

			
			}
			
			
				try {
					db.end();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		
		
	
	
	
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		ConnectionManager connection_manager = new ConnectionManager();
		new ConsoleViewConnection(connection_manager);
		TweetManifold modele = new TweetManifold();
		new ConsoleViewTweet(modele);
    }



	@Override
	public void update(Observable arg0, Object arg1) {
		
	}
	
}
