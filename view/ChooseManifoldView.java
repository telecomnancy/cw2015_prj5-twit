package view;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.*;
import model.*;

public class ChooseManifoldView extends JPanel{
	private MainWindow parent;
	private ArrayList<Couple<Integer, String>> tweets;
	private JList<String> tweet_list, user_list;
	private JButton new_tweet_btn, new_user_btn, choose_tweet_btn, choose_user_btn, delete_user_btn, delete_tweet_btn;
	private JPanel tweet_control, user_control;
	private JScrollPane tweet_pane, user_pane;
	private DefaultListModel<String> list_model;
	
	public ChooseManifoldView(MainWindow p){
		parent=p;
		tweet_list = new JList<String>();
		user_list = new JList<String>();
		tweets = new ArrayList<Couple<Integer, String>>();
		
		try {
			tweets = parent.database().getTweetSearch();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		list_model = new DefaultListModel<>();
		
		for(int i=0; i<tweets.size(); i++){
			Couple<Integer, String> s = tweets.get(i);
			if(s.getF()!=null && !s.getF().equals("null") && !s.getF().equals("")){
				list_model.addElement(s.getF());
			}else{
				tweets.remove(i);
			}
		}
		
		tweet_list.setModel(list_model);
		
		new_tweet_btn = new JButton("Nouveau");
		new_user_btn = new JButton("Nouveau");
		choose_tweet_btn = new JButton("S�lectionner");
		choose_user_btn = new JButton("S�lectionner");
		delete_user_btn = new JButton("Supprimer");
		delete_tweet_btn = new JButton("Supprimer");
		
		new_tweet_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setTweetPanel();
			}
		});
		
		new_user_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setUserPanel();
			}
		});
		
		choose_tweet_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					ArrayList<Couple<Integer, String>> search = parent.database().getTweetSearch();
					int i = tweet_list.getSelectedIndex();
					
					if(i!=-1){
						int index = search.get(i).getE();
						TweetManifold manifold = parent.database().extraire(index);
						parent.setTweetPanel(manifold);
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		delete_tweet_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = tweet_list.getSelectedIndex();
				
				if(i!=-1){
					try {
						parent.database().deleteTable(tweets.get(i).getE());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, "La recherche a bien �t� supprim�e !");
					tweets.remove(i);
					list_model.remove(i);
				}
			}
		});
		
		tweet_control = new JPanel();
		user_control = new JPanel();
		
		tweet_control.setLayout(new GridLayout(1, 3));
		user_control.setLayout(new GridLayout(1, 3));
		
		tweet_control.add(choose_tweet_btn);
		tweet_control.add(delete_tweet_btn);
		tweet_control.add(new_tweet_btn);
		
		user_control.add(choose_user_btn);
		user_control.add(delete_user_btn);
		user_control.add(new_user_btn);
		
		tweet_pane = new JScrollPane(tweet_list);
		user_pane = new JScrollPane(user_list);
		
		JPanel user_panel = new JPanel();
		user_panel.setLayout(new BorderLayout());
		
		JPanel tweet_panel = new JPanel();
		tweet_panel.setLayout(new BorderLayout());
		
		user_panel.add(new JLabel("Analyse utilisateur"), BorderLayout.NORTH);
		user_panel.add(user_pane, BorderLayout.CENTER);
		user_panel.add(user_control, BorderLayout.SOUTH);
		
		tweet_panel.add(new JLabel("Analyse de tweet"), BorderLayout.NORTH);
		tweet_panel.add(tweet_pane, BorderLayout.CENTER);
		tweet_panel.add(tweet_control, BorderLayout.SOUTH);
		
		JPanel cont = new JPanel();
		cont.setLayout(new BorderLayout());
		cont.add(tweet_panel, BorderLayout.EAST);
		cont.add(user_panel, BorderLayout.WEST);
		
		add(cont);
	}
}
