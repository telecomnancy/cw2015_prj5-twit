package view;

import java.io.IOException;
import java.util.*;
import twitter4j.*;
import twitter4j.auth.*;
import model.*;

public class ConsoleViewConnection {
	private Scanner console;
	private ConnectionManager connection_manager;
	
	public ConsoleViewConnection(ConnectionManager compte) throws ClassNotFoundException, IOException{
		this.connection_manager=compte;
		this.console=new Scanner(System.in);
		try {
			ManageClientLogin();
		} catch (LoginError e) {
			System.out.println("erreur de connexion");}		
	}
	
	public void ManageClientLogin() throws LoginError{
		String s = "";
		String name="";
		RequestToken req = null;

			while(!(s.equals("y") || s.equals("n"))){
				System.out.println("Voulez vous connecter ou cr�er un nouveau log? y/n");
				s=this.console.nextLine();
			}
				
				if(s.equals("y")){
					while(!(s.equals("c") || s.equals("n"))){
					System.out.println("Pour se connecter taper c.Pour cr�er un compte taper n.");
					s=this.console.nextLine();
					 
						if (s.equals("connexion")||s.equals("c")){
							System.out.println("se connecter");
							System.out.println("Entrer l'adresse relative du fichier ");
							name = this.console.nextLine();
							
							
							try {
								AccessToken tokl= connection_manager.load(name);
								TwitterFactory.getSingleton().setOAuthAccessToken(tokl);
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						
						else if (s.equals("nouveau")||s.equals("n")){
							System.out.println("cr�er un compte");
							
							try {
								req=connection_manager.getRequestToken();
							} catch (TwitterException e) {
								e.printStackTrace();
							}
							System.out.println(req.getAuthorizationURL());
							System.out.println("copiez le pin d'authentification");
							String pin=this.console.nextLine();
							
							try {
								 AccessToken tok=connection_manager.getAccessToken(req, pin);
								 
								 System.out.println("entrer le chemin o� vous souhaiter stocker votre fichier log");
								 String chemin=this.console.nextLine();
								 
								 try {
									connection_manager.save(tok,chemin);
									TwitterFactory.getSingleton().setOAuthAccessToken(tok);
								} catch (IOException e) {
									e.printStackTrace();
								}
							} catch (TwitterException e) {
								e.printStackTrace();
							}
							
						}
				}
				}
			
			while(!(s.equals("q")||s.equals("quitter"))){
				System.out.println("tweet|t : utiliser la vue tweet -"
						+" user|u : vue utilisateur - quitter|q : quitter le programme"); 
				s=this.console.nextLine();
				s.toLowerCase();

			
			 if (s.equals("tweet")||s.equals("t")){
				System.out.println("Vue tweet");
				TweetManifold modele = new TweetManifold();
				new ConsoleViewTweet(modele);
			}
			else if (s.equals("user")||s.equals("u")){
				System.out.println("Vue utilisateur");
				UserManifold modele = new UserManifold();
				new ConsoleViewUser(modele);
			}
			else if (s.equals("quitter")||s.equals("q")){
				System.out.println("May the Force be With you young Rey");
				

			}
			
			else{
				System.out.println("tweet|t : utiliser la vue tweet -"
						+" user|u : vue utilisateur - quitter|q : quitter le programme");
				}
		}	
	}
	
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		ConnectionManager connection_manager = new ConnectionManager();
		new ConsoleViewConnection(connection_manager);
    }
}

