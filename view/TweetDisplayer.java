package view;

import java.util.ArrayList;
import javax.swing.JTextPane;
import model.Tweet;


/**
 * 
 * Cette classe permet d'afficher un tweet dans la liste.
 *
 */
public class TweetDisplayer extends JTextPane{
	
	//El�ments que ll'on veut r�cup�rer dans le tweet.
	private String author;
	private String date;
	private String hashtags;
	private String text;
	
	public TweetDisplayer(Tweet tweet){
		super();
		
		//On met le texte en forme.
		author = "@"+tweet.getAuthor();
		date = tweet.getDate().toString();
		ArrayList<String> hash = tweet.getHashtags();
		//derbi base de donn�e
		
		hashtags="";
		for (int i = 0; i < hash.size(); i++) {
			hashtags+="#"+hash.get(i);
			if(i!=hash.size()-2) hashtags+=", ";
		}
		
		text = "From "+ author + ", " 
				+ date + "\n"
				+ hashtags + "\n\n"
				+ tweet.getText();
		
		this.setText(text);
	}
}
