package view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import model.UserManifold;
import twitter4j.TwitterException;

public class UserManifoldPanel extends JPanel{
	private MainWindow parent;
	
	private JButton opt_btn, ok_btn, stop_btn, goback_btn;
	private JTextField search_field;
	private JPanel control_panel;
	
	private LanguageNetwork network_panel;
	private UserStatPanel stat_panel;
	
	private UserManifold manifold;
	
	public UserManifoldPanel(MainWindow p){
		parent=p;
		
		opt_btn = new JButton("Options");
		ok_btn = new JButton("Lancer");
		stop_btn = new JButton("Arr�ter");
		goback_btn = new JButton("Accueil");
		search_field = new JTextField();
		
		control_panel = new JPanel();
		control_panel.setLayout(new GridLayout(1, 4));
		control_panel.add(search_field);
		control_panel.add(opt_btn);
		control_panel.add(ok_btn);
		control_panel.add(stop_btn);
		control_panel.add(goback_btn);
		
		ok_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					manifold.stalk(search_field.getText());
				} catch (TwitterException e1) {
					JOptionPane.showMessageDialog(null,"Cet utilisateur n'existe pas", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		goback_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				parent.setChoosePanel();
			}
		});
		
		manifold = new UserManifold();
		
		network_panel = new LanguageNetwork();
		manifold.addObserver(network_panel);
		
		stat_panel = new UserStatPanel();
		manifold.addObserver(stat_panel);
		
		setLayout(new BorderLayout());
		add(control_panel, BorderLayout.NORTH);
		
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		container.add(stat_panel, BorderLayout.NORTH);
		container.add(network_panel, BorderLayout.SOUTH);
		
		JScrollPane pane = new JScrollPane();
		pane.setViewportView(container);
		pane.setPreferredSize(new Dimension(1100, 600));
		
		add(pane, BorderLayout.SOUTH);
	}
}
