package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;


import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.data.general.DefaultPieDataset;
import model.UserManifold;

public class LanguageNetwork extends JPanel implements Observer{
	private DefaultPieDataset dataset_followers, dataset_followed;
	private JFreeChart pie_followers, pie_followeds;
	private ChartPanel panel_followers, panel_followeds;
	private static final int NB_DISPLAYED = 5;
	
	public LanguageNetwork(){
		dataset_followed = new DefaultPieDataset();
		dataset_followers= new DefaultPieDataset();
		dataset_followed.setValue("Langue", 1.0);
		dataset_followers.setValue("Langue", 1.0);
		
		pie_followers = ChartFactory.createPieChart3D("Langue des followers", dataset_followers, true, true, false);
		pie_followeds = ChartFactory.createPieChart3D("Langue des followeds", dataset_followed, true, true, false);
		panel_followers = new ChartPanel(pie_followers);
		panel_followeds = new ChartPanel(pie_followeds);
		
		add(panel_followers);
		add(panel_followeds);
	}

	@Override
	public void update(Observable o, Object arg) {
		UserManifold ump = (UserManifold) o;
		
		//Lorsqu'on met � jour, on supprime les anciennes donn�es
		dataset_followed.clear();
		dataset_followers.clear();
		
		for(Object obj : dataset_followed.getKeys()){
			String s = (String) obj;
			dataset_followed.remove(s);
		}
		
		for(Object obj : dataset_followers.getKeys()){
			String s = (String) obj;
			dataset_followers.remove(s);
		}

		HashMap<String, Double> data_followed = ump.getFollowedLang();
		ArrayList<String> ordered_followed = new ArrayList<String>();
		
		for(String lang : data_followed.keySet()){			
			if(ordered_followed.size() < NB_DISPLAYED){
				ordered_followed.add(lang);
			}else{
				for (int i = 0; i < ordered_followed.size(); i++) {
					if(!ordered_followed.contains(lang) && data_followed.get(ordered_followed.get(i)).doubleValue() < data_followed.get(lang)){
						ordered_followed.set(i, lang);
					}
				}
			}
		}
		
		HashMap<String, Double> data_followers = ump.getFollowersLang();
		ArrayList<String> ordered_followers = new ArrayList<String>();
		
		for(String lang : data_followers.keySet()){			
			if(ordered_followers.size() < NB_DISPLAYED){
				ordered_followers.add(lang);
			}else{
				for (int i = 0; i < ordered_followers.size(); i++) {
					if(!ordered_followers.contains(lang) && data_followers.get(ordered_followers.get(i)).doubleValue() < data_followers.get(lang)){
						ordered_followers.set(i, lang);
					}
				}
			}
		}
		
		double total = 0;
		for(int  i =0; i<ordered_followed.size(); i++){
				String current = ordered_followed.get(i);
				double value = data_followed.get(current);
				try{
					dataset_followed.setValue(current, value);
				}catch(Throwable e){}
				total+=value;
		}
		
		dataset_followed.setValue("Autre", 1.0 - total);
		
		total = 0;
		for(int  i =0; i<ordered_followers.size(); i++){
			String current = ordered_followers.get(i);
			double value = data_followers.get(current);
			try{
				dataset_followers.setValue(current, value);
			}catch(Throwable e){}
			total+=value;
		}
		dataset_followers.setValue("Autre", 1.0 - total);	
		
		panel_followeds.repaint();
		panel_followers.repaint();
	}
}
