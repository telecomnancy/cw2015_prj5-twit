package view;

import java.awt.event.*;
import javax.swing.*;
import model.*;

public class MainWindow extends JFrame{
	private TweetManifoldPanel tweet_panel;
	private UserManifoldPanel user_panel;
	private ChooseManifoldView choose_panel;
	private ConnectionPanel connection_panel;
	private DatabaseManager manager;
	
	public MainWindow(){
		try {
			manager = new DatabaseManager();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setIconImage(new ImageIcon("./resources/PapuchTweetLogo.png").getImage());
		
		this.pack();
		this.setVisible(true);
		
		WindowListener exitListener = new WindowAdapter() {

		    @Override
		    public void windowClosing(WindowEvent e) {
		        try {
					manager.end();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
		    }
		};
		addWindowListener(exitListener);
		
		//On choisit le fichier d'authentification
		JFileChooser fileChooser = new JFileChooser();
		int result = -1;
		
		setConnectionPanel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		System.out.println(System.getProperties().get("user.dir"));
		MainWindow m = new MainWindow();
	}
	
	public void setConnectionPanel(){
		connection_panel = new ConnectionPanel(this);
		add(connection_panel);
		this.pack();
		this.setTitle("Gestionnaire de connexion");
	}
	
	public void setTweetPanel(TweetManifold manifold){
		tweet_panel = new TweetManifoldPanel(this, manifold);
		this.remove(choose_panel);
		choose_panel = null;
		this.add(tweet_panel);
		this.pack();
		this.setTitle("Collecteur de tweets");
	}
	
	public void setTweetPanel(){
		tweet_panel = new TweetManifoldPanel(this);
		this.remove(choose_panel);
		choose_panel = null;
		this.add(tweet_panel);
		this.pack();
		this.setTitle("Collecteur de tweets");
	}
	
	public void setChoosePanel(){
		choose_panel = new ChooseManifoldView(this);
		if(tweet_panel != null)
			this.remove(tweet_panel);
		else if(user_panel != null)
			this.remove(user_panel);
		else if(connection_panel != null)
			this.remove(connection_panel);
		
		user_panel = null;
		tweet_panel = null;
		connection_panel = null;
		
		add(choose_panel);
		this.pack();
		this.setTitle("Gestionnaire de sauvegardes");
	}
	
	public void setUserPanel(){
		user_panel = new UserManifoldPanel(this);
		this.remove(choose_panel);
		choose_panel = null;
		this.add(user_panel);
		this.pack();
		this.setTitle("Analyse d'utilisateur");
	}
	
	public DatabaseManager database(){
		return manager;
	}
}
