package view;

import java.util.*;
import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.data.general.DefaultPieDataset;
import model.LanguageLooter;

public class LanguagePie extends JPanel implements Observer{
	private DefaultPieDataset dataset;
	private JFreeChart pie;
	private ChartPanel panel;
	private static final int NB_DISPLAYED = 5;
	
	public LanguagePie() {
		dataset = new DefaultPieDataset();
		dataset.setValue("Langue", 1.0);
		
		pie = ChartFactory.createPieChart3D("Langue des tweets", dataset, true, true, false);
		panel = new ChartPanel(pie);
		add(panel);
	}

	@Override
	public void update(Observable o, Object arg) {
		ArrayList<String> ordered = new ArrayList<String>();
		LanguageLooter ll = (LanguageLooter) o;
		
		synchronized(LanguageLooter.class){
			//Lorsqu'on met � jour, on supprime les anciennes donn�es
			dataset.clear();
			for(Object obj : dataset.getKeys()){
				String s = (String) obj;
				dataset.remove(s);
			}

		HashMap<String, Double> data = ll.toPercent();
			
			for(String lang : data.keySet()){			
				if(ordered.size() < NB_DISPLAYED){
					ordered.add(lang);
				}else{
					for (int i = 0; i < ordered.size(); i++) {
						if(!ordered.contains(lang) && data.get(ordered.get(i)).doubleValue() < data.get(lang)){
							ordered.set(i, lang);
						}
					}
				}
			}
			
			double total = 0;
			for(int  i =0; i<ordered.size(); i++){
					String current = ordered.get(i);
					double value = data.get(current);
					try{
						dataset.setValue(current, value);
					}catch(Throwable e){}
					total+=value;
			}
					
			dataset.setValue("Autre", 1.0 - total);
			panel.repaint();
		}
	}
}
