package view;

import java.util.*;
import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import model.PopularityLooter;

public class PopularityView extends JPanel implements Observer{
	private JFreeChart lineChart;
	private ChartPanel panel;
	private DefaultCategoryDataset dataset;
	
	public PopularityView() {
		lineChart = ChartFactory.createLineChart("Popularit�", "Temps","Nombre de tweets", dataset, PlotOrientation.VERTICAL,true,true,false);
		panel = new ChartPanel(lineChart);
		add(panel);
	}

	@Override
	public void update(Observable o, Object arg) {
		PopularityLooter looter = (PopularityLooter) o;
		HashMap<Date, Integer> data = looter.data();
		
		if (data.isEmpty()){
			return;
		}
		
		//On applique les changements � la vue
		dataset = new DefaultCategoryDataset();
		
		int i;
		
		ArrayList<Date> tri = new ArrayList<Date>();
		
		for(Date d : data.keySet()){
			
			i = 0;
			
			while(tri.size() > i && d.after(tri.get(i))){
				i++;
			}
			if (i < tri.size()){
				tri.add(i,d);
			}else{
				tri.add(d);
			}
			
		}
		
		Date current = (Date) tri.get(0).clone();
		long jump;
		int number;
		
		while(!current.after(tri.get(tri.size() - 1 )) ){
			jump = current.getTime();
			current.setTime(jump + (60*60*1000));
			number = 0;
			
			if (data.containsKey(current)){
				number = data.get(current);
			}
			
			dataset.addValue((Number) number, "Nombre de tweets", current.getTime());
			
		}
		
		lineChart = ChartFactory.createLineChart("Popularit�", "Temps","Nombre de tweets", dataset, PlotOrientation.VERTICAL,true,true,false);
		panel.setChart(lineChart);
	}

}
