package view;

import java.util.*;
import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;

import model.HashtagLooter;

public class CorrelationView extends JPanel implements Observer{
	
	/**
	 * Donn�es servant � l'affichage de la vue.
	 */
	private CategoryDataset dataset;
	
	/**
	 * Composant graphique de l'histogramme.
	 */
	private JFreeChart bar;
	
	/**
	 * Panel contenant le composant graphique.
	 */
	private ChartPanel panel;
	
	/**
	 * 
	 */
	private ArrayList<String> columnKeys;
	
	/**
	 * Tableau des valeurs � afficher.
	 * La vue attend un tableau � deux dimensions pour afficher des barres 
	 * compartiment�es, cependant nous n'en aurons pas besoin.
	 */
	private ArrayList<ArrayList<Integer>> values;
	
	private static final int NB_DISPLAY = 7;
	
	public CorrelationView(){
		columnKeys = new ArrayList<String>();
		values=new ArrayList<ArrayList<Integer>>();
		values.add(new ArrayList<Integer>());
		
		dataset = DatasetUtilities.createCategoryDataset(new String[]{"hashtag"}, new String[]{"Autre"}, new double[][]{{100}});
		
		bar = ChartFactory.createBarChart3D("Corr�lation entre hashtags", "Hashtag", "Nombre de hashtags", dataset);
		panel = new ChartPanel(bar);
		add(panel);
	}

	@Override
	public void update(Observable o, Object arg){
		
		HashtagLooter cl = (HashtagLooter) o;
		HashMap<String, Integer> data = cl.data();
		columnKeys = new ArrayList<String>();
		values = new ArrayList<ArrayList<Integer>>();
		values.add(new ArrayList<Integer>());
		
		for(String hashtag : data.keySet()){
			columnKeys.add(hashtag);
			values.get(0).add(data.get(hashtag));
		}
		
		ArrayList<String> ordered_hash = new ArrayList<String>();
		ArrayList<Integer> ordered_val = new ArrayList<Integer>();
		
		//On ordonne les hashtags en fonction de leur nombre
		for(int i=0; i<values.get(0).size();i++){
			int bound = ordered_hash.size();
			
			for (int j = 0; j <= bound; j++) {
				
				if(j == ordered_hash.size()){
					ordered_hash.add(columnKeys.get(i));
					ordered_val.add(values.get(0).get(i));
				}else{
					if(values.get(0).get(i) > ordered_val.get(j) && !ordered_hash.contains(columnKeys.get(i))){
						ordered_val.add(j, values.get(0).get(i));
						ordered_hash.add(j, columnKeys.get(i));
					}
				}
			}
		}
		
		ArrayList<String> selection = new ArrayList<String>();
		int nb_view = (ordered_hash.size() < CorrelationView.NB_DISPLAY ) ? ordered_hash.size() : CorrelationView.NB_DISPLAY;
		double[][] tab = new double[1][nb_view];
		for (int i = 0; i < tab[0].length; i++) {
			tab[0][i] = ordered_val.get(i);
			selection.add(ordered_hash.get(i));
		}
		
		if(selection.size() != 0){
			dataset = DatasetUtilities.createCategoryDataset(new String[]{"Hashtag"}, (String []) selection.toArray(new String[1]), tab);
			bar = ChartFactory.createBarChart3D("Corr�lation entre hashtags", "Hashtag", "Nombre de hashtags", dataset);
			panel.setChart(bar);
		}
	}
}
