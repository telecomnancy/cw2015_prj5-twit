package view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import model.*;
import twitter4j.TwitterException;

/**
 * 
 * Panel graphique principal de la vue de r�colte des tweets.
 *
 */
public class TweetManifoldPanel extends JPanel{
	
	/**
	 * Composant d'affichage de la liste des tweets.
	 */
	private TweetPanel tweet_panel;
	
	/**
	 * Mod�le de la r�coolte de tweets.
	 */
	private TweetManifold manifold;
	
	/**
	 * Boutons de contr�le.
	 */
	private JButton options_btn, launch_btn, stop_btn, save_btn, goback_btn;
	
	/**
	 * Panel de contr�le contenant les boutons ad�quats
	 */
	private JPanel control_panel;
	
	/**
	 * Champ de recherche
	 */
	private JTextField search_field;
	
	/**
	 * Panel affichant la corr�lation entre le tweet et d'autres hashtags
	 */
	private CorrelationView correlation_view;
	
	/**
	 * Affichage de la fr�quence d'utilisation du tweet sur une p�riode.
	 */
	private PopularityView popularity_view;
	
	/**
	 * Affichage de la r�partition des langues pour cette recherche.    
	 */
	private LanguagePie pie;
	
	private MainWindow parent;
	
	public TweetManifoldPanel(MainWindow p, TweetManifold manif){
		parent = p;
		
		manifold = manif;
		this.setLayout(new BorderLayout());
		
		setControlPanel();
		setListener();
		
		tweet_panel = new TweetPanel(manifold);
		manifold.addObserver(tweet_panel);
		manifold.notifyChanges();
		
		pie = new LanguagePie();
		correlation_view = new CorrelationView();
		popularity_view = new PopularityView();
		
		try {
			manifold.getLooter(new LanguageLooter()).addObserver(pie);
			manifold.getLooter(new LanguageLooter()).notifyChanges();
			manifold.getLooter(new HashtagLooter()).addObserver(correlation_view);
			manifold.getLooter(new HashtagLooter()).notifyChanges();
			manifold.getLooter(new PopularityLooter()).addObserver(popularity_view);
			manifold.getLooter(new PopularityLooter()).notifyChanges();
		} catch (UnknownLooterException e) {
			JOptionPane.showMessageDialog(this,"Erreur : "+e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
		
		JPanel container = new JPanel();
		container.setLayout(new GridLayout(2, 2));
		
		container.add(tweet_panel);
		container.add(pie);
		container.add(correlation_view);
		container.add(popularity_view);
		
		JScrollPane pane = new JScrollPane();
		pane.setViewportView(container);
		pane.setPreferredSize(new Dimension(1100, 600));
		
		this.add(pane, BorderLayout.SOUTH);
	}
		
	public TweetManifoldPanel(MainWindow p){
		this(p, new TweetManifold());
	}
	
	public void setControlPanel(){
		control_panel = new JPanel();
		options_btn = new JButton("Options");
		launch_btn = new JButton("Lancer");
		save_btn = new JButton("Sauvegarder");
		stop_btn = new JButton("Arreter");
		goback_btn = new JButton("Accueil");
		search_field = new JTextField();
		
		control_panel.setLayout(new GridLayout(1, 4));
		control_panel.add(search_field);
		control_panel.add(options_btn);
		control_panel.add(launch_btn);
		control_panel.add(stop_btn);
		control_panel.add(save_btn);
		control_panel.add(goback_btn);
		
		this.add(control_panel, BorderLayout.NORTH);
	}
	
	public void setListener(){
		launch_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0){
				if(!search_field.getText().isEmpty() && search_field.getText() != null && search_field.getText() != ""){
					tweet_panel.clean();
					manifold.setQuery(search_field.getText());
					
					try {
						try {
							manifold.collect();
						} catch (NoResultException e) {
							JOptionPane.showMessageDialog(null,"Erreur : "+e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
						}
					} catch (TwitterException e) {
						JOptionPane.showMessageDialog(null,"Erreur : "+e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
					} catch (TooManyRequestsException e) {
						JOptionPane.showMessageDialog(null,"Erreur : "+e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
					}
				}else
					JOptionPane.showMessageDialog(null,"Il faut rentrer une recherche !", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
		});
		
		save_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					try {
						parent.database().write(manifold);
						JOptionPane.showMessageDialog(null, "Votre recherche a bien �t� sauvegard�e dans la base de donn�es !", "Enregistrement r�alis�", JOptionPane.INFORMATION_MESSAGE);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
			}
		});
		
		goback_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setChoosePanel();
			}
		});
	}
}
