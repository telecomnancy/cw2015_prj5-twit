package view;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import model.UserManifold;

public class UserStatPanel extends JPanel implements Observer{
	private JLabel created_lab, lang_lab, followers_lab, followed_lab, nb_statuses_lab;
	private JLabel follower_followers_lab, follower_followed_lab, follower_nb_statuses_lab;
	private JLabel followed_followers_lab, followed_followed_lab, followed_nb_statuses_lab;
	private JPanel user_info, followers_info, followeds_info;
	
	public UserStatPanel() {
		created_lab = new JLabel("Date de cr�ation : ");
		lang_lab = new JLabel("Langue : ");
		followers_lab=new JLabel("Nb followers : ");
		followed_lab=new JLabel("Nb followed : "); 
		nb_statuses_lab=new JLabel("Nb statuses : ");
		
		user_info = new JPanel();
		user_info.setLayout(new GridLayout(6, 1));
		user_info.add(new JLabel("Statistiques de l'utilisateur"));
		user_info.add(created_lab);
		user_info.add(lang_lab);
		user_info.add(followers_lab);
		user_info.add(followed_lab);
		user_info.add(nb_statuses_lab);
		
		user_info.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		this.setLayout(new GridLayout(1, 3));
		
		follower_followers_lab = new JLabel("Nombre de followers : ");
		follower_followed_lab = new JLabel("Nombre de followed : ");
		follower_nb_statuses_lab = new JLabel("Nombre de statuts : ");
		
		followed_followers_lab = new JLabel("Nombre de followers : ");
		followed_followed_lab = new JLabel("Nombre de followed : ");
		followed_nb_statuses_lab = new JLabel("Nombre de statuts : ");
		
		followeds_info = new JPanel();
		followeds_info.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		followeds_info.setLayout(new GridLayout(4, 1));
		followeds_info.add(new JLabel("Moyennes followeds"));
		followeds_info.add(followed_followed_lab);
		followeds_info.add(followed_followers_lab);
		followeds_info.add(followed_nb_statuses_lab);
		
		followers_info = new JPanel();
		followers_info.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		followers_info.setLayout(new GridLayout(4, 1));
		followers_info.add(new JLabel("Moyennes followers"));
		followers_info.add(follower_followers_lab);
		followers_info.add(follower_followed_lab);
		followers_info.add(follower_nb_statuses_lab);
		
		add(user_info);
		add(followers_info);
		add(followeds_info);
	}

	@Override
	public void update(Observable o, Object arg) {
		UserManifold ump = (UserManifold) o;
		
		String[] info = ump.getUserinformations();
		
		created_lab.setText("Date de cr�ation : "+info[0]);
		lang_lab.setText("Langue : "+info[1]);
		followers_lab.setText("Nb followers : "+info[2]);
		followed_lab.setText("Nb followed : "+info[3]); 
		nb_statuses_lab.setText("Nb statuses : "+info[4]);
		
		String[] flwrs = ump.getUserFollowedStatistics();	
		followed_followers_lab.setText("Nombre de followers : "+flwrs[0]);
		followed_followed_lab.setText("Nombre de followed : "+flwrs[1]);
		followed_nb_statuses_lab.setText("Nombre de statuts : "+flwrs[2]);
		
		String[] flwds = ump.getUserFollowersStatistics();
		follower_followers_lab.setText("Nombre de followers : "+flwds[0]);
		follower_followed_lab.setText("Nombre de followed : "+flwds[1]);
		follower_nb_statuses_lab.setText("Nombre de statuts : "+flwds[2]);
	}
}
