package view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.*;
import model.ConnectionManager;
import twitter4j.*;
import twitter4j.auth.*;

/**
 * 
 * Ce panel affiche la liste des connexions utilisables par l'utilisateur.
 * Il peut en cr�er une, en supprimer une et se connecter.
 *
 */
public class ConnectionPanel extends JPanel{
	
	/**
	 * JFrame contenant le connection panel.
	 */
	private MainWindow parent;
	
	/**
	 * Boutons de contr�le pour cr�er une nouvelle connexion, en supprimer un et se connecter.
	 */
	private JButton load_btn, new_btn;
	
	public ConnectionPanel(MainWindow p){
		parent = p;
		
		load_btn = new JButton("Charger un fichier d'authentification");
		new_btn = new JButton("Cr�er un fichier d'authentification");
		
		load_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//On choisit le fichier d'authentification
				JFileChooser fileChooser = new JFileChooser();
				int result = -1;
					fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
					result = fileChooser.showOpenDialog(null);
					
					if(result == JFileChooser.APPROVE_OPTION){
						File token = fileChooser.getSelectedFile();
						try {
							AccessToken tk = ConnectionManager.load(token.getAbsolutePath());
							TwitterFactory.getSingleton().setOAuthAccessToken(tk);
							parent.setChoosePanel();
						} catch (ClassNotFoundException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}else{
						JOptionPane.showMessageDialog(null,"Erreur : Vous devez choisir un fichier de connexion", "Erreur", JOptionPane.ERROR_MESSAGE);
					}
				}
		});
		
		
		new_btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					RequestToken r_t = ConnectionManager.getRequestToken();
					JPanel text = new JPanel();
					text.add(new JLabel("Entrez le lien suivant dans votre navigateur : \n"));
					text.add(new JTextField(r_t.getAuthorizationURL()));
					JPanel pan = new JPanel();
					JLabel txt2 = new JLabel("Puis entrez le code pin retourn� par Twitter.");
					pan.add(text);
					pan.add(txt2);
					
					String pin = JOptionPane.showInputDialog(new_btn, pan, "Connexion", JOptionPane.INFORMATION_MESSAGE);
					
					if(pin != null && !pin.equals("")){
					AccessToken token = ConnectionManager.getAccessToken(r_t, pin);
					JFileChooser fileChooser = new JFileChooser();
					int result = -1;
					
						fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
						result = fileChooser.showSaveDialog(null);
						
						if(result == JFileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null){
							File file = fileChooser.getSelectedFile();
							try {
								ConnectionManager.save(token, file.getAbsolutePath());
								TwitterFactory.getSingleton().setOAuthAccessToken(token);
								parent.setChoosePanel();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}else{
							JOptionPane.showMessageDialog(null,"Erreur : Vous devez choisir un emplacement", "Erreur", JOptionPane.ERROR_MESSAGE);
						}				
					}else{
						JOptionPane.showMessageDialog(null,"Erreur : Vous devez entrer le code pin", "Erreur", JOptionPane.ERROR_MESSAGE);
					}
					
				} catch (TwitterException e1) {
					JOptionPane.showMessageDialog(new_btn,"Erreur : "+e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		add(new_btn);
		add(load_btn);
	}		
}
