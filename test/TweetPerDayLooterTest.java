package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;

import model.ConnectionManager;
import model.RetweetLooter;
import model.TweetPerDayLooter;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;

public class TweetPerDayLooterTest {
	private TweetPerDayLooter tpdl;
	private AccessToken at=null;
	private ArrayList<Status> tweets=null;

	private Twitter t =TwitterFactory.getSingleton();
	/**
	 * Booleen servant a se souvenir qu'on s'est deja authentifie 
	 */
	private static boolean done=false;
	
	/**
	 * Cant work TOO MANY REQUESTS #TWITTER #15REQUETES C'EST TROP
	 */
	public TweetPerDayLooterTest() {
		this.tpdl=new TweetPerDayLooter();
		if (done==false) {
			done=true;
			// Authentification a Twitter
			System.out.println("Entrez le chemin absolu de votre ficher d'authentification .auth");
			Scanner console = new Scanner(System.in);
			String s = console.nextLine();
			try {
				at = ConnectionManager.load(s);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			t.setOAuthAccessToken(at);
		}	
		//Chargement de tweets sur l'user Trabarak
		User trabarak=null;
		try {
			trabarak = t.showUser("Trabarak");
		} catch (TwitterException e1) {
			e1.printStackTrace();
		}
		//UserList ul = new UserList();
		long trabaID = trabarak.getId();
		
		Paging page = new Paging(1,100);
		ResponseList<Status> r=null;
		tweets = new ArrayList<Status>();
		for (int i=1;i<5;i++) {
			try {
				r = t.getUserTimeline(trabaID,page);
				
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			for(int j=0;j<r.size();j++) {
				tweets.add(r.get(j));
			}
			page.setPage(i+1);
		}		
	}

	@Test
	public void testTake() {
		for(int i=0;i<10;i++) {
			tpdl.take(tweets.get(i));
		}
		
	}

	@Test
	public void testData() {
		fail("Not yet implemented");
	}

}
