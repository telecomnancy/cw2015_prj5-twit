package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Scanner;

import org.junit.Test;

import model.ConnectionManager;
import model.UserManifold;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class UserManifoldTest {
	private UserManifold um;
	private AccessToken at=null;

	private Twitter t =TwitterFactory.getSingleton();
	
	/**
	 * Booleen servant a se souvenir qu'on s'est deja authentifie 
	 */
	private static boolean done=false;
	
	public UserManifoldTest() {
		this.um=new UserManifold();
		if (done==false) {
			done=true;
			// Authentification a Twitter
			System.out.println("Entrez le chemin absolu de votre ficher d'authentification .auth");
			Scanner console = new Scanner(System.in);
			String s = console.nextLine();
			try {
				at = ConnectionManager.load(s);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			t.setOAuthAccessToken(at);
		}	
	}
	@Test
	public void testStalk() {
		try {
			um.stalk("Trabarak");
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		assertTrue(um.getCurrentUser()!=null);
		assertTrue(um.getFollowedLang()!=null);
		assertTrue(um.getFollowersLang()!=null);
		assertTrue(um.getLooters().size()==3);
		assertTrue(um.getUserFollowedStatistics()!=null);
		assertTrue(um.getUserFollowersStatistics()!=null);
		assertTrue(um.getUserinformations()[1].equals("fr"));
		//System.out.println(um.getUserinformations()[2]);
		assertTrue(Integer.parseInt(um.getUserinformations()[2])>50);
		assertTrue(Integer.parseInt(um.getUserinformations()[3])>200);
		assertTrue(Integer.parseInt(um.getUserinformations()[4])>1900);
	}

	@Test
	public void testSetCurrentUser() {
		um.setCurrentUser("charoy");
		assertTrue(um.getCurrentUser().equals("charoy"));
	}


}
