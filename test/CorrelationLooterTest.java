package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;

import model.*;
import twitter4j.HashtagEntity;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class CorrelationLooterTest {
	private HashtagLooter cl;
	private AccessToken at=null;
	private ArrayList<Status> tweets=null;

	private Twitter t =TwitterFactory.getSingleton();
	private static boolean done=false;
	
	public CorrelationLooterTest() {
		this.cl=new HashtagLooter();
		if (done==false) {
			done=true;
			System.out.println("Entrez le chemin absolu de votre ficher d'authentification .auth");
			Scanner console = new Scanner(System.in);
			String s = console.nextLine();
			try {
				at = ConnectionManager.load(s);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			t.setOAuthAccessToken(at);
		}
		
		Query query = new Query();
		query.setCount(20);
		query.setQuery("#CodingWeek");
		QueryResult result=null;
		try {
			result = t.search(query);
		} catch (TwitterException te) {
			te.printStackTrace();
		}
		ArrayList<Status> temp =(ArrayList<Status>)result.getTweets();
		this.tweets=temp;
	}
	
	@Test
	public void testTake() {
		cl.take(this.tweets.get(0));
		HashtagEntity[] he = this.tweets.get(0).getHashtagEntities();
		int count =0;
		ArrayList<String> s = new ArrayList<String>();
		for (int i=0;i<he.length;i++) {
			if (s.contains(he[i].getText())) {
				count++;
			} else {
				s.add(he[i].getText());
				count++;
			}
		}
		int total=0;
		for (String l : cl.data().keySet()) {
			total+=cl.data().get(l);
		}
		assertEquals(count,total);
	}

	@Test
	public void testClear() {
		for (int i=0;i<tweets.size();i++) {
			cl.take(this.tweets.get(i));
		}
		cl.clear();
		assertTrue(cl.data().size()==0);
	}

	@Test
	public void testData() {
		int total=0;
		int count =0;
		
		for (int j=0;j<this.tweets.size();j++) {
			cl.take(this.tweets.get(j));
			HashtagEntity[] he = this.tweets.get(j).getHashtagEntities();
			
			ArrayList<String> s = new ArrayList<String>();
			for (int i=0;i<he.length;i++) {
				if (s.contains(he[i].getText())) {
					count++;
				} else {
					s.add(he[i].getText());
					count++;
				}
			}
		}
		for (String l : cl.data().keySet()) {
			total+=cl.data().get(l);
		}
		assertSame(count,total);
	}

}
