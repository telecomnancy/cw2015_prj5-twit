package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;
import model.*;
import twitter4j.*;
import twitter4j.auth.AccessToken;

public class PopularityLooterTest {
	private PopularityLooter pl;
	private AccessToken at=null;
	private ArrayList<Status> tweets=null;

	private Twitter t =TwitterFactory.getSingleton();
	private static boolean done=false;
	
	public PopularityLooterTest() {
		this.pl=new PopularityLooter();
		if (done==false) {
			done=true;
			System.out.println("Entrez le chemin absolu de votre ficher d'authentification .auth");
			Scanner console = new Scanner(System.in);
			String s = console.nextLine();
			try {
				at = ConnectionManager.load(s);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			t.setOAuthAccessToken(at);
		}
			Query query = new Query();
			query.setCount(20);
			query.setQuery("#CodingWeek");
			QueryResult result=null;
			try {
				result = t.search(query);
			} catch (TwitterException te) {
				te.printStackTrace();
			}
			ArrayList<Status> temp =(ArrayList<Status>)result.getTweets();
			this.tweets=temp;
	}

	@Test
	public void testTake() {
		pl.take(this.tweets.get(0));
		assertSame(1,pl.data().size());
	}

	@Test
	public void testData() {
		for (int i=0;i<tweets.size();i++) {
			pl.take(this.tweets.get(i));
		}
		assertSame(this.tweets.size(),pl.data().size());
		//fail("Not yet implemented");
	}
	
	@Test
	public void testClear() {
		for (int i=0;i<tweets.size();i++) {
			pl.take(this.tweets.get(i));
		}
		pl.clear();
		assertTrue(pl.data().size()==0);
	}
}
