package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;

import model.ConnectionManager;
import model.LocationLooter;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class LocationLooterTest {
	private LocationLooter ll;
	private AccessToken at=null;
	private ArrayList<Status> tweets=null;

	private Twitter t =TwitterFactory.getSingleton();
	private static boolean done=false;
	
	public LocationLooterTest() {
		this.ll=new LocationLooter();
		if (done==false) {
			done=true;
			System.out.println("Entrez le chemin absolu de votre ficher d'authentification .auth");
			Scanner console = new Scanner(System.in);
			String s = console.nextLine();
			try {
				at = ConnectionManager.load(s);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			t.setOAuthAccessToken(at);
		}
		
		Query query = new Query();
		query.setCount(20);
		query.setQuery("#CodingWeek");
		QueryResult result=null;
		try {
			result = t.search(query);
		} catch (TwitterException te) {
			te.printStackTrace();
		}
		ArrayList<Status> temp =(ArrayList<Status>)result.getTweets();
		this.tweets=temp;
	}

	@Test
	public void testTake() {
		ll.take(this.tweets.get(0));
		if (this.tweets.get(0).getGeoLocation()==null){
			assertSame(0,ll.data().size());
		} else {
			assertSame(1,ll.data().size());
		}
	}
	@Test
	public void testData() {
		int count=0;
		for (int i=0;i<tweets.size();i++) {
			ll.take(this.tweets.get(i));
			if (this.tweets.get(i).getGeoLocation()!=null){
				count++;
			}
		}
		assertSame(count,ll.data().size());
	}
	
	@Test
	public void testClear() {
		for (int i=0;i<tweets.size();i++) {
			ll.take(this.tweets.get(i));
		}
		ll.clear();
		assertTrue(ll.data().size()==0);
	}
}
