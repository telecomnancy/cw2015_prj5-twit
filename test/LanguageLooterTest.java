package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.junit.Test;

import model.ConnectionManager;
import model.LanguageLooter;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class LanguageLooterTest {
	private LanguageLooter ll;
	private AccessToken at=null;
	private ArrayList<Status> tweets=null;

	private Twitter t =TwitterFactory.getSingleton();
	private static boolean done=false;
	public LanguageLooterTest() {
		this.ll=new LanguageLooter();
		if (done==false) {
			done=true;
			System.out.println("Entrez le chemin absolu de votre ficher d'authentification .auth");
			Scanner console = new Scanner(System.in);
			String s = console.nextLine();
			try {
				at = ConnectionManager.load(s);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			t.setOAuthAccessToken(at);
		}
		
		Query query = new Query();
		query.setCount(20);
		query.setQuery("#CodingWeek");
		QueryResult result=null;
		try {
			result = t.search(query);
		} catch (TwitterException te) {
			te.printStackTrace();
		}
		ArrayList<Status> temp =(ArrayList<Status>)result.getTweets();
		this.tweets=temp;
	}

	@Test
	public void testTake() {
		ll.take(this.tweets.get(0));
		if (this.tweets.get(0).getLang()==null){
			assertSame(0,ll.data().size());
		} else {
			assertSame(1,ll.data().size());
		}
	}

	@Test
	public void testData() {
		int count=0;
		ArrayList<String> s = new ArrayList<String>();
		for (int i=0;i<tweets.size();i++) {
			ll.take(this.tweets.get(i));
			if (!s.contains(this.tweets.get(i).getLang())){
				count++;
				s.add(this.tweets.get(i).getLang());
			}
		}
		assertSame(count,ll.data().size());
	}

	@Test
	public void testToPercent() {
		for (int i=0;i<tweets.size();i++) {
			ll.take(this.tweets.get(i));
		}
		double count =0;
		HashMap<String, Double> hm = ll.toPercent();
		for (String l : hm.keySet()) {
			count+=hm.get(l);
		}
		assertTrue(count>0.99 && count<1.01);
	}
	
	@Test
	public void testClear() {
		for (int i=0;i<tweets.size();i++) {
			ll.take(this.tweets.get(i));
		}
		ll.clear();
		assertTrue(ll.data().size()==0);
	}
}
