LUNDI:
Connexion twitter (graphique et console), collecte des tweets,affichage des tweets (graphique et console),première
analyse de tweets (langues et nombre d'apparition) avec affichage graphique et console.

MARDI: 
Interface utilisateur (graphique et console), 1ère analyse utilisateur,géolocalisation et analyse réseau 
(tweet et retweet)

MERCREDI:
Implémentation de la base de données modifications des options de recherche et d'analyse de la collecte 
et de la fouille utilisateur

JEUDI : collecte approfondie pour représenter la toile des hashtags, analyse utilisateur approfondie pour
représenter le réseau de l'utilisateur

VENDREDI: Debug, cryptage des clés de connexion, finition des fonctionnalités non-terminées,
ajout de nouvelles fonctionnalités si nouvelles idées, peaufinage des anciennes fonctionnalités.

Le Rapport de conception est disponible au format .pdf.
Le manuel d'utilisateur est aussi disponible au format .pdf

Les boutons options et arrêter n'ont pas pu être implémentés à cause de la mauvaise utilisation des threads dans l'interface graphique.