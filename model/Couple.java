package model;

public class Couple<E,F> {
	private E e1;
	private F e2;
	
	public Couple(E e1, F e2) {
		this.e1=e1;
		this.e2=e2;
	}

	public E getE() {
		return e1;
	}

	public void setE(E e1) {
		this.e1 = e1;
	}

	public F getF() {
		return e2;
	}

	public void setF(F e2) {
		this.e2 = e2;
	}
	
	@Override
	public boolean equals(Object o){
		Couple<E, F> c = (Couple<E, F>)o;
		return c.e1.equals(e1) && c.e2.equals(e2);
	}
}
