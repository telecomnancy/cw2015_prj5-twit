package model;

import java.util.*;
import twitter4j.User;

public abstract class Stalker extends Observable{
	
	public abstract void take(User user);
	public abstract HashMap<?,?> data ();
	public abstract String toString();
	public boolean equals (Object o){
		Stalker stalker = (Stalker)o;
		return this.toString() == stalker.toString();
	}
}
