package model;

public class TweetLooterDeletionException extends Exception {
	
	public TweetLooterDeletionException() {
		super();
	}
	
	public TweetLooterDeletionException(String s) {
		super(s);
	}
}
