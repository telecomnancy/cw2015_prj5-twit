package model;

import java.util.HashMap;

import twitter4j.Status;

/**
 * Analyse et stocke les langues utilis�es dans les tweets 
 * 
 *
 */
public class LanguageLooter extends Looter{
	/**
	 * Compte le nombre de tweets analys�s (permet de calculer des pourcentages)
	 */
	private int nb_messages;
	
	/**
	 * Contient les donn�es r�cup�r�es par LanguageLooter 
	 * String : Langue d'un Status
	 * Integer : nombre de status dont la langue est le "String"
	 */
	private volatile HashMap<String,Integer> data;
	
	/**
	 * Constructeur de LanguageLooter
	 */
	public LanguageLooter(){
		super();
		data = new HashMap<String,Integer>();
		nb_messages=0;
	}
	
	/**
	 * Constructeur de LanguageLooter avec param�tres
	 * @param newdata data � mettre dans le Looter
	 * @param nb_messages nb_messages analys�s correspondant � newdata
	 */
	public LanguageLooter(HashMap<String, Integer> newdata, int nb_messages){
		this.data = newdata;
		this.nb_messages = nb_messages;
	}
	
	/**
	 * Extrait la langue d'un tweet
	 */
	@Override
	public void take(Status status) {
		
		synchronized (LanguageLooter.class) {
			String language = status.getLang();
			
			if (language == null){
				return;
			}

			if (data.containsKey(language)){
				int contain = data.get(language) + 1;
				data.put(language, contain);
			}else{
				data.put(language, 1);
			}
			
			nb_messages++;
			
			setChanged();
			notifyObservers();
		}
	}

	@Override
	public String toString() {
		return "LanguageLooter";
	}

	/**
	 * Retourne les donn�es stock�es dans le looter
	 */
	@Override
	public HashMap<String,Integer> data() {
		return this.data;
	}
	
	/**
	 * Convertit les donn�es stock�es dans le looter en pourcentages 
	 * @return
	 */
	public HashMap<String, Double> toPercent(){
		HashMap<String, Double> per = new HashMap<String, Double>();
		
		for(String l : data.keySet()){
			per.put(l, data.get(l)/(nb_messages+0.0));
		}
		
		return per;
	}
	
	/**
	 * Remise � zero des donn�es contenues dans data
	 */
	@Override
	public void clear() {
		data = new HashMap<String, Integer>();
		nb_messages=0;
	}
}
