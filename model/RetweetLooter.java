package model;

import java.util.HashMap;
import twitter4j.*;

public class RetweetLooter extends Looter {
	private HashMap<Long,Integer> data;
	private Twitter t=TwitterFactory.getSingleton();
	
	public RetweetLooter() {
		super();
		this.data = new HashMap<Long,Integer>();
	}
	
	public RetweetLooter(HashMap<Long, Integer> data) {
		super();
		this.data=data;
	}
	
	/**
	 * Analyse des retweets selon les diff�rents users
	 */
	@Override
	public void take(Status status) {
		long id = status.getId();
		IDs teub=null;
		try {
			teub = t.getRetweeterIds(id, -1);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		System.out.println(teub.getIDs().length);
		long[] teub2 = teub.getIDs();
		for(int i=0;i<teub2.length;i++){
			if (data.containsKey(teub2[i])){
				this.data.put(teub2[i], this.data.get(teub2[i]) + 1);
			}else{
				data.put(teub2[i], 1);
			}
		}
		
		setChanged();
		notifyObservers();
	}

	@Override
	public String toString() {
		
		return "RetweetLooter";
	}

	/**
	 * Retourne les donn�es stock�es dans le looter
	 */
	@Override
	public HashMap<Long, Integer> data() {
		return data;
	}
	
	@Override
	public void clear() {
		data = new HashMap<Long, Integer>();
	}

}
