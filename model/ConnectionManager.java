package model;

import java.io.*;
import twitter4j.auth.*;
import twitter4j.*;

/**
 * 
 * This class allows to manage connections with the Twitter API
 *
 */
public class ConnectionManager{
	
	private static boolean done = false;
	private static RequestToken request_token;
	
	/**
	 * Charge une cl� d'acc�s pr�alablement sauvegard�e � partir du pseudo utilisateur
	 * Chaque cl� utilisateur est enregistr�e sous le mod�le pseudo.auth
	 * 
	 * @param screen_name Le pseudo de l'utilisateur
	 * @return la cl� d'acc�s au compte utilisateur
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	
	public static AccessToken load(String file) throws IOException, ClassNotFoundException{
		//We put the keys
			try {
				if(!done){
					ConnectionManager.getRequestToken();
					done = true;
				}
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		
		FileInputStream src = new FileInputStream(new File(file));
		ObjectInputStream reader = new ObjectInputStream(src);
		AccessToken token = (AccessToken) reader.readObject();
		
		return token;
	}
	
	/**
	 * Sauvegarde une cl� d'acc�s sous le mod�le path.auth
	 * @param token Cl� d'acc�s � sauvegarder. Elle contient le pseudo de l'utilisateur.
	 * @throws IOException 
	 */
	public static void save(AccessToken token, String path) throws IOException{
		FileOutputStream dest = new FileOutputStream(path+".auth");
		ObjectOutputStream writer = new ObjectOutputStream(dest);
		writer.writeObject(token);
	}
	
	/**
	 * Cr�e une cl� d'acc�s � partir d'un request token et du pin fourni par Twitter pour authentifier l'utilisateur
	 * initialize() must be called before to get the request token
	 * @return La cl� d'authentification
	 * @throws TwitterException 
	 */
	public static AccessToken getAccessToken(RequestToken token, String pin) throws TwitterException{
		Twitter twitter = TwitterFactory.getSingleton();
		AccessToken access_token = twitter.getOAuthAccessToken(token, pin);
		
		return access_token;
	}
	
	public static RequestToken getRequestToken() throws TwitterException{
		
		if(!done){
			Twitter twitter = TwitterFactory.getSingleton();
			twitter.setOAuthConsumer("fPs3Es5vX54fqZ3iGWEDMvgQp", "LXqMfj4EP9X4QRiQPJpHg4zjHD3S0JePf5KqkfdBsXrtbp5Epj");
			request_token = twitter.getOAuthRequestToken();
			done = true;
		}
		
		return request_token;
	}
}
