package model;

import java.awt.HeadlessException;
import java.util.*;
import javax.swing.JOptionPane;
import twitter4j.*;

public class TweetManifold extends Observable implements Runnable{
	
	/**
	 * Contient une liste de looters s�lectionn�es par l'utilisateur.
	 * Le 0 est et doit toujours �tre un TweetLooter
	 */
	private static final int NOMBRE_MAX_TWEETS=1000;
	private Query query;
	private ArrayList<Status> tweets;
	private ArrayList<Tweet> conversion;
	private ArrayList<Looter> looters;
	private int nb_pages;
	private String search_string;
	
	/**
	 * Constructeur de TweetManifold = ensemble des donn�es relatives aux tweets
	 * @param tweet Ensemble de tweets 
	 */
	public TweetManifold() {
		this.query = new Query();
		this.tweets = new ArrayList<Status>();
		this.looters = new ArrayList<Looter>();
		conversion = new ArrayList<Tweet>();
		
		looters.add(new LanguageLooter());
		looters.add(new HashtagLooter());
		looters.add(new PopularityLooter());
	}
	
	public TweetManifold(ArrayList<Looter> looters){
		this.query = new Query();
		this.tweets = new ArrayList<Status>();
		this.looters = looters;
	}
	
	/**
	 * 
	 * @param status
	 */
	private void extract(Status status) {
		for (int i=nb_pages * 100;i<this.looters.size();i++) {
			Looter l = looters.get(i);
				l.take(status);
		}
	}
	
	private void cleanLooters(){
		for (int i=0;i<this.looters.size();i++) {
			looters.get(i).clear();;
		}
	}
	
	/**
	 * Fonction qui effectue la recherche associ�e � une requ�te
	 * @param query requ�te effectu�e par l'utilisateur (ex : #Papuche)
	 * @throws TwitterException 
	 * @throws TooManyRequestsException 
	 * @throws NoResultException 
	 */
	public void collect() throws TwitterException, TooManyRequestsException, NoResultException {
		cleanLooters();
		nb_pages=0;
		
		this.tweets = new ArrayList<Status>();
		this.conversion = new ArrayList<Tweet>();

		this.query.setCount(100);

		Twitter t = TwitterFactory.getSingleton();
		QueryResult result=null;
				
		do{
			this.query.setCount(100);
			
			// Je lance ma requ�te
			try {
				result = t.search(this.query);
			} catch (TwitterException te) {
				te.printStackTrace();
			}

			ArrayList<Status> temp = (ArrayList<Status>)result.getTweets();
			
			if(temp.size()==0){
				throw new NoResultException();
			}
			
			//J'extrais les tweets
			for (int i=0;i<temp.size();i++){
				this.tweets.add(temp.get(i));
				this.conversion.add(new Tweet(temp.get(i)));
				this.extract(tweets.get(i));
				setChanged();
				notifyObservers();
			}
			this.query = result.nextQuery();
			
		}while(result.hasNext() && ++nb_pages * 100 < NOMBRE_MAX_TWEETS);
	}
	
	public void addLooter(Looter l) {
		this.looters.add(l);
	}
	
	/**
	 * Retourne le looter correspondant au type de looter en entr�e
	 * @param l Un nouveau Looter qui sert � d�finir le type de Looter recherch� dans l'ArrayList
	 * @return
	 */
	public Looter getLooter(Looter l) throws UnknownLooterException {
		int looter_index = this.looters.indexOf(l);
		Looter looter_searched = null;
		if (looter_index==-1){
			throw new UnknownLooterException("Cette option n'a pas �t� selectionn�e (Looter Introuvable)");
		} else {
			looter_searched = this.looters.get(looter_index);
		}
		return looter_searched;
	}
	
	/**
	 * Supprime un looter de la liste des looters (on enl�ve un type de donn�es)
	 * @param l
	 * @throws UnknownLooterException
	 * @throws TweetLooterDeletionException 
	 */
	public void removeLooter(Looter l) throws UnknownLooterException, TweetLooterDeletionException {
		int looter_index = this.looters.indexOf(l);
		if (looter_index==-1) {
			throw new UnknownLooterException("Ce looter n'existe pas. Rien n'a �t� supprim�");
		} else {
			this.looters.remove(looter_index);
		}	
	}
	
	public ArrayList<Status> getCollection() {
		return this.tweets;
	}
	
	public ArrayList<Looter> getLooters(){
		return this.looters;
	}
	
	public void setQuery(String q){
		query = new Query();
		query.setQuery(q);
		search_string = q;
	}
	
	public Query getQuery(){
		return this.query;
	}
	
	public ArrayList<Tweet> getTweets(){
		return conversion;
	}
	
	public void setConversion(ArrayList<Tweet> conv){
		this.conversion=conv;
	}
	
	public void addConversion(Tweet t){
		this.conversion.add(t);
	}

	@Override
	public void run() {
		try {
				try {
					try {
						collect();
					} catch (TooManyRequestsException e) {
						JOptionPane.showMessageDialog(null,"Erreur : "+e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
					} catch (NoResultException e) {
						JOptionPane.showMessageDialog(null,"Erreur : "+e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
					}
				} catch (HeadlessException e) {
					e.printStackTrace();
				}
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}
	
	public String getQueryString(){
		return search_string;
	}
	
	public void notifyChanges(){
		setChanged();
		notifyObservers();
	}
}
