package model;

import java.util.*;
import twitter4j.*;

public class UserManifold extends Observable{
	private String currentUser="";
	/**
	 * Liste des followers d'un user
	 */
	private User[] followers;
	
	/**
	 * Liste des followeds d'un user
	 */
	private User[] followeds;
	
	/**
	 * D'ou viennent les followed d'un user
	 */
	private FollowedPosStalker followedStalker;
	
	/**
	 * D'ou viennent les followers d'un user
	 */
	private FollowerPosStalker followerStalker;
	
	/**
	 * Liens entre utilisateurs : echelle subjective
	 * Integer 1 : niveau d'influence de A sur B
	 * Integer 2 : niveau d'influence de B sur A
	 */
	private HashMap<Couple<String,String>, Couple<Integer,Integer>> user_links;
	
	/**
	 * Ensemble des looters de donn�es relatif a un user
	 */
	private ArrayList<Looter> looters;
	
	/**
	 * Informations de base d'un utilisateur : creeLe, Langue, nbFollowers, nbFollowedBy, nbStatuses
	 */
	private String[] user_informations = new String[5];
	
	/**
	 * Informations concernant les followers de l'utilisateur : moyenneFollowers, moyenneFollowed, moyenneNBStatuses
	 */
	private String[] userFollowersStatistics = new String[3];
	
	/**
	 * Informations concernant les followeds de l'utilisateur : moyenneFollowers, moyenneFollowed, moyenneNBStatuses
	 */
	private String[] userFollowedStatistics = new String[3];
	
	/**
	 * R�partition des langues des followers
	 */
	private HashMap<String,Double> followersLang;
	
	/**
	 * R�partition des langues des followeds
	 */
	private HashMap<String,Double> followedLang;

	public UserManifold(){
		looters = new ArrayList<Looter>();
		user_links = new HashMap<Couple<String, String>, Couple<Integer, Integer>>();
		
		looters.add(new QuoteLooter());
		//looters.add(new RetweetLooter());
		looters.add(new HashtagLooter());
		looters.add(new TweetPerDayLooter());
	}
	
	public void stalk(String username) throws TwitterException{
		this.setCurrentUser(username);
		Twitter t= TwitterFactory.getSingleton();
		long target = t.searchUsers(username, 1).get(0).getId();
		//System.out.println(target+"");
		User user = null;
		user = t.showUser(username);
		
		// Remplissage user_infos
		fillUserInformations(user);
		
		
		int countfollowed=0;
		int countfollowers=0;
		// var userFollowersStatistics
		double fwers =0.0;
		double fders =0.0;
		double status =0.0;
		
		// var userFollowedsStatistics
		double fwers2 =0.0;
		double fders2 =0.0;
		double status2 =0.0;
		
		// sizes
		int followersListsize =0;
		int followedsListsize=0;
		
		// On extrait les followers et les followeds de l'user entr� en entr�e
		PagableResponseList<User> followersList = t.getFollowersList(target,-1);
		PagableResponseList<User> followedsList = t.getFriendsList(target,-1);
		
		followers = (User[]) followersList.toArray(new User[user.getFollowersCount()]);
		followeds = (User[]) followedsList.toArray(new User[user.getFriendsCount()]);
		
		
		for (int l=0;l<3;l++) {
			//System.out.println(followersList.size());
			// REMPLISSAGE followersLang
				this.followersLang = new HashMap<String,Double>();
				for (int i=0;i<followersList.size();i++) {
					if (this.followersLang.containsKey(followersList.get(i).getLang())) {
						this.followersLang.put(followersList.get(i).getLang(), this.followersLang.get(followersList.get(i).getLang())+1.0);
					} else {
						//System.out.println("coucou");
						this.followersLang.put(followersList.get(i).getLang(), 1.0);
					}
					countfollowers++;
				}
				
			
			// REMPLISSAGE followedLang
	
			this.followedLang = new HashMap<String,Double>();
			for (int i=0;i<followedsList.size();i++) {
				if (this.followedLang.containsKey(followedsList.get(i).getLang())) {
					this.followedLang.put(followedsList.get(i).getLang(), this.followedLang.get(followedsList.get(i).getLang())+1.0);
				} else {
					this.followedLang.put(followedsList.get(i).getLang(), 1.0);
				}
				countfollowed++;
			}
			
			// REMPLISSAGE userFollowersStatistics
			//System.out.println(followersList.size());
			for (int i=0;i<followersList.size();i++) {
				//System.out.println(followersList.get(i).getFollowersCount());
				fwers+=followersList.get(i).getFollowersCount();
				fders+=followersList.get(i).getFriendsCount();
				status+=followersList.get(i).getStatusesCount();
			}
			followersListsize+=followersList.size();
			
			
			// REMPLISSAGE userFollowedsStatistics
			for (int i=0;i<followedsList.size();i++) {
				fwers2+=followedsList.get(i).getFollowersCount();
				fders2+=followedsList.get(i).getFriendsCount();
				status2+=followedsList.get(i).getStatusesCount();
			}
			followedsListsize+=followedsList.size();
			
			followersList= t.getFollowersList(target, followersList.getNextCursor());
			followedsList= t.getFollowersList(target, followedsList.getNextCursor());
		}
		
		
		//pourcentage followersLang
		for (String s : this.followersLang.keySet()) {
			this.followersLang.put(s, this.followersLang.get(s)/countfollowers);
		}
		//pourcentage followedLang
		for (String s : this.followedLang.keySet()) {
			this.followedLang.put(s, this.followedLang.get(s)/countfollowed);
		}
		// moyennes followers
		if (followedsListsize!=0) {
			// moyenne followers
			fwers = fwers/(followersListsize);
			// moyenne followeds
			fders = fders/(followersListsize);
			// moyenne nombre statuts publi�s
			status = status/(followersListsize);
		}
		// moyenne followed
		if (followedsListsize!=0) {
			// moyenne followers
			fwers2 = fwers2/(followedsListsize);
			// moyenne followeds
			fders2 = fders2/(followedsListsize);
			// moyenne nombre statuts publi�s
			status2 = status2/(followedsListsize);
		}
		
		// remplissage userFollowersStatistics
		this.userFollowersStatistics[0]=fwers+"";
		this.userFollowersStatistics[1]=fders+"";
		this.userFollowersStatistics[2]=status+"";
		
		// remplissage userFollowedStatistics
		this.userFollowedStatistics[0]=fwers2+"";
		this.userFollowedStatistics[1]=fders2+"";
		this.userFollowedStatistics[2]=status2+"";
		
		//extractFolloweds();
		//extractFollowers();
		
		// On remplit les looters
		
		long userID = user.getId();
		Paging page = new Paging(1,100);
		ResponseList<Status> r = null;
		
		for (int i=1;i<2;i++) {
			try {
				// Pour chaque page on recupere les tweets du user
				r = t.getUserTimeline(userID,page);
				
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			// pour chaque tweet
			for(int j=0;j<r.size();j++) {
				// on remplit chaque looter
				for (int k=0;k<looters.size();k++) {
					looters.get(k).take(r.get(j));
				}
				setChanged();
				notifyObservers();
			}
			page.setPage(i+1);
		}
		
		// LOOTERS REMPLIS
		
		this.fillUserLinks(user, t);
	}
	
	public String[] getUserinformations() {
		return user_informations;
	}

	public void setUserinformations(String[] user_informations) {
		this.user_informations = user_informations;
	}

	public User[] followers(){
		return this.followers;
	}
	
	public User[] followeds(){
		return this.followeds;
	}
	
	public FollowedPosStalker getfollowedStalker(){
		return followedStalker;
	}
	
	public FollowerPosStalker getfollowerStalker(){
		return followerStalker;
	}
	
	private void extractFollowers(){
		followerStalker=new FollowerPosStalker();
		
		for (int i = 0; i < this.followers.length; i++){
			followerStalker.take(this.followers[i]);
		}
	}
	
	private void extractFolloweds(){
		followedStalker=new FollowedPosStalker();
		
		for (int i = 0; i< this.followeds.length; i++){
			followedStalker.take(followeds[i]);
		}
	}
	
	/**
	 * Fonction qui permet de remplir la hashmap d'influences de User a partir des contenus des looters
	 * @param user
	 */
	private void fillUserLinks(User user, Twitter t) {
	/*	
		// REMPLISSAGE PAR QUOTELOOTER
		HashMap<String, Integer>  data=null;
		for (int i=0;i<looters.size();i++) {
			if (looters.get(i).toString().equals("QuoteLooter")) {
				data = (HashMap<String, Integer>) looters.get(i).data();
			}
		}
		for(String s : data.keySet()) {
			Couple<String,String> currentCouple = new Couple<String,String>(user.getScreenName(),s);
			// si le couple User(A,B) existe deja 
			if (user_links.containsKey(currentCouple)) {
				// On recupere ses valeurs
				Couple<Integer,Integer> c = user_links.get(currentCouple);
				// on met a jour la valeur d'influence de B sur A (+2 par nombre de fois que A a cite B)
				c.setF(c.getF()+2*data.get(s));
				user_links.put(currentCouple, c); 
			} else {
				Couple<Integer,Integer> c = new Couple<Integer,Integer>(0,2*data.get(s));
				user_links.put(currentCouple, c);
			}
		}
		// FIN REMPLISSAGE PAR QUOTELOOTER
		
		data.clear();
		data=new HashMap<String,Integer>();
		// REMPLISSAGE PAR RETWEETLOOTER
		HashMap<Long, Integer>  data2=null;
		for (int i=0;i<looters.size();i++) {
			if (looters.get(i).toString().equals("RetweetLooter")) {
				data2 = (HashMap<Long, Integer>) looters.get(i).data();
			}
			ArrayList<Long> keys = new ArrayList<Long>(data2.keySet());
			ArrayList<Integer> values = new ArrayList<Integer>(data2.values());
			
			// Tri
			for (int j=0;j<keys.size();j++) {
				int v = values.get(j);
				long l = keys.get(j);
				int k = j;
				while (k>0 && values.get(k-1)<l) {
					values.set(k,values.get(k-1));
					keys.set(k,keys.get(k-1));
				}
				values.set(k,v);
				keys.set(k, l);
			}
			// Je pr�pare mes parametres a envoyer a twitter pour recup les users (screenname)
			long[] listID = new long[keys.size()];
			for (int k=0;k<keys.size();k++) {
				listID[k]=keys.get(k);
			}
			ResponseList<User> i_believe=null;
			try {
				i_believe = t.lookupUsers(listID);
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			ArrayList<String> screen_names = new ArrayList<String>();
			for (int k = 0;k<i_believe.size();k++) {
				screen_names.add(i_believe.get(k).getScreenName());
			}
			// On a nos couples screen_name/value
			for (int j=0;j<values.size();j++) {
				data.put(screen_names.get(j), values.get(j));
			}
			
			
		}
		
		
		for (String s : data.keySet()) {
			Couple<String,String> currentCouple = new Couple<String,String>(user.getScreenName(),s);
			if (user_links.containsKey(currentCouple)) {
				Couple<Integer,Integer> c = user_links.get(currentCouple);
				// on met a jour la valeur d'influence de A sur B (+5 par nombre de fois que A a cite B)
				c.setE(c.getE()+5*data.get(currentCouple));
				user_links.put(currentCouple, c);
			} else {
				user_links.put(currentCouple, new Couple<Integer,Integer>(5*data.get(currentCouple),0));
			}
		} */
	}
	
	private void fillUserInformations(User user) {
		this.user_informations[0]=user.getCreatedAt().toString();
		this.user_informations[1]=user.getLang();
		this.user_informations[2]=Integer.toString(user.getFollowersCount());
		this.user_informations[3]=Integer.toString(user.getFriendsCount());
		this.user_informations[4]=Integer.toString(user.getStatusesCount());
	}
	
	public void addLooter(Looter l) {
		this.looters.add(l);
	}
	
	/**
	 * Retourne le looter correspondant au type de looter en entr�e
	 * @param l Un nouveau Looter qui sert � d�finir le type de Looter recherch� dans l'ArrayList
	 * @return
	 */
	public Looter getLooter(Looter l) throws UnknownLooterException {
		int looter_index = this.looters.indexOf(l);
		Looter looter_searched = null;
		if (looter_index==-1){
			throw new UnknownLooterException("Cette option n'a pas �t� selectionn�e (Looter Introuvable)");
		} else {
			looter_searched = this.looters.get(looter_index);
		}
		return looter_searched;
	}
	
	/**
	 * Supprime un looter de la liste des looters (on enl�ve un type de donn�es)
	 * @param l
	 * @throws UnknownLooterException
	 * @throws TweetLooterDeletionException 
	 */
	public void removeLooter(Looter l) throws UnknownLooterException, TweetLooterDeletionException {
		int looter_index = this.looters.indexOf(l);
		if (looter_index==-1) {
			throw new UnknownLooterException("Ce looter n'existe pas. Rien n'a �t� supprim�");
		} else {
			this.looters.remove(looter_index);
		}	
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	

	public ArrayList<Looter> getLooters() {
		return looters;
	}

	public void setLooters(ArrayList<Looter> looters) {
		this.looters = looters;
	}
	
	public HashMap<Couple<String, String>, Couple<Integer, Integer>> getInteractions(){
		return user_links;
	}
	
	public HashMap<String, Double> getFollowersLang() {
		return followersLang;
	}

	public void setFollowersLang(HashMap<String, Double> followersLang) {
		this.followersLang = followersLang;
	}

	public HashMap<String, Double> getFollowedLang() {
		return followedLang;
	}

	public void setFollowedLang(HashMap<String, Double> followedLang) {
		this.followedLang = followedLang;
	}

	public String[] getUserFollowersStatistics() {
		return userFollowersStatistics;
	}

	public void setUserFollowersStatistics(String[] userFollowersStatistics) {
		this.userFollowersStatistics = userFollowersStatistics;
	}

	public String[] getUserFollowedStatistics() {
		return userFollowedStatistics;
	}

	public void setUserFollowedStatistics(String[] userFollowedStatistics) {
		this.userFollowedStatistics = userFollowedStatistics;
	}
	
	

}

