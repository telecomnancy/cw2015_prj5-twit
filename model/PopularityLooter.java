package model;

import java.util.*;
import twitter4j.Status;

/**
 * Analyse et retiens la popularité des tweets
 * 
 */
public class PopularityLooter extends Looter{
	private HashMap<Date, Integer> data;
	
	public PopularityLooter(){
		super();
		data = new HashMap<Date,Integer>();
	}
	
	public PopularityLooter(HashMap<Date, Integer> newdata){
		this.data = newdata;
	}
	/**
	 * Analyse la popularit� d'un tweet rentr� en argument
	 */
	@Override
	public void take(Status status) {
		Date current_date = status.getCreatedAt();
		Long time = current_date.getTime();
		current_date = new Date(time-time%(60*60*1000));
		
		if (data.containsKey(current_date)){
			int contain = this.data.get(current_date) + 1;
			this.data.put(current_date, contain);
		}else{
			data.put(current_date, 1);
		}
		
		setChanged();
		notifyObservers();
	}

	@Override
	public String toString() {
		return "PopularityLooter";
	}

	/**
	 * Retourne les donn�es stock�es dans le looter
	 */
	@Override
	public HashMap<Date, Integer> data() {
		return data;
	}
	
	@Override
	public void clear() {
		data = new HashMap<Date, Integer>();
	}

}
