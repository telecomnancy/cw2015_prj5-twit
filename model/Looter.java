package model;

import java.util.*;
import twitter4j.*;

public abstract class Looter extends Observable {
	
	/**
	 * 
	 * @param status
	 */
	public abstract void take(Status status);
	
	/**
	 * Chaine unique a chaque sous-classe qui permet d'identifier quelle sous classe
	 */
	public abstract String toString();
	
	/**
	 * 
	 * @return Donn�es r�cup�r�es par le Looter
	 */
	public abstract HashMap<?,?> data();
	
	/**
	 * permet de tester si une instance d'une sous-classe est egale a une autre instance
	 */
	public boolean equals(Object o) {
		Looter looter = (Looter)o;
		return this.toString().equals(looter.toString());
	}
	
	/**
	 * Remise � zero des donn�es contenues dans le Looter
	 */
	public abstract void clear();
	
	/**
	 * Permet de notifier les vues
	 */
	public void notifyChanges(){
		setChanged();
		notifyObservers();
	}
}
