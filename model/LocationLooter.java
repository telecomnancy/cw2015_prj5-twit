package model;

import java.util.HashMap;
import twitter4j.*;


/**
 * Cherche et retiens la localisation des tweets
 * 
 *
 */
public class LocationLooter extends Looter{
	
	/**
	 * Contient les donn�es r�cup�r�es par data
	 */
	private HashMap<GeoLocation, Integer> data;
	
	/**
	 * Constructeur de LocationLooter
	 */
	public LocationLooter(){
		super();
		data = new HashMap<GeoLocation,Integer>();
	}
	
	/**
	 * Constructeur de LocationLooter avec param�tres
	 * @param newdata data � mettre dans le LocationLooter
	 */
	public LocationLooter(HashMap<GeoLocation, Integer> newdata){
		this.data = newdata;
	}
	
	/**
	 * Extrait les donn�es de localisation du looter
	 */
	public void take(Status status){
		
		GeoLocation geo = status.getGeoLocation();
		
		if (geo == null){
			Place place = status.getPlace();
			System.out.println(place);
			GeoLocation[][] approx_zone = place.getGeometryCoordinates();
			int count = 0;
			double lat = 0.0;
			double longit = 0.0;
			for (int i=0;i<approx_zone.length;i++) {
				for(int j=0;j<approx_zone[i].length;j++) {
					lat+=approx_zone[i][j].getLatitude();
					longit+=approx_zone[i][j].getLongitude();
					count++;
				}
			}
			lat = lat/count;
			longit=longit/count;
			GeoLocation approx_geo = new GeoLocation(lat,longit);
			if (data.containsKey(approx_geo)){
				int contain = data.get(approx_geo) + 1;
				data.put(approx_geo, contain);
			}else{
				data.put(approx_geo, 1);
			}
		} else {
			if (data.containsKey(geo)){
				int contain = data.get(geo) + 1;
				data.put(geo, contain);
			}else{
				data.put(geo, 1);
			}
		}
		setChanged();
		notifyObservers();
	}
	
	
	@Override
	public String toString() {
		return "LocationLooter";
	}

	/**
	 * Retourne les donn�es contenues dans le looter
	 */
	@Override
	public HashMap<GeoLocation, Integer> data() {
		return this.data;
	}
	
	/**
	 * Remise � z�ro de data
	 */
	@Override
	public void clear() {
		data = new HashMap<GeoLocation, Integer>();
	}
}
