package model;

public class UnknownLooterException extends Exception {
	
	public UnknownLooterException() {
		super();
	}
	
	public UnknownLooterException(String s) {
		super(s);
	}
}
