package model;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import twitter4j.*;


/**
 * Cette classe a pour objectif de gérer les entrees et sorties entre la base de donnees
 * et le logiciel
 * En outre c'est cette classe qui extrait des structures de donnes du logiciel, les informations relatifs aux tweets.
 * Cette classe a pour objectif de creer a partir de la base de donnes, les structures de donnees exploitables par le logiciel, et qu'il transformera en outil statistique. 
 * @author nicolas
 *
 */
public class DatabaseManager {
	
	private String name = "database";
	private String dbURL = "jdbc:derby:"+name+";create=true";
	private Connection connexion;
	private Statement twitters;
	
	/**
	 * Ce construction a pour objectif, outre d'instancier un DatabaseManager, d'ouvrir le serveur derby, et d'initialiser la connexion avec celui-ci.  
	 * @throws Exception
	 */
	public DatabaseManager() throws Exception{
		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Class.forName(driver).newInstance();
		
		connexion = DriverManager.getConnection(dbURL);
		twitters = connexion.createStatement();
	}
	
	/**
	 * Termine la liaison avec le serveur de base de donnees. Cette fonction renvoie une exception, ce qui est normal dans la fermeture d'un serveur derby (voir document derby).
	 * @throws Exception
	 */
	public void end() throws Exception{
		twitters.close();
		
		try{
			DriverManager.getConnection("jdbc:derby:"+name+";shutdown=true");
		}
		catch(SQLException e){
			System.out.println("Exception de fermeture : Everything is OK");
		}
		
		connexion.close();
	}
	
	/**
	 * Execute toutes les commandes SQL d'une fichier en parmatres, situé dans le dossier SQL.
	 * @param file_name Nom du fichier.
	 */
	public void command_file(String file_name){
		String file ="SQL/"+file_name;
		
		try{
			InputStream descripteur = new FileInputStream(file); 
			InputStreamReader pointeur = new InputStreamReader(descripteur);
			BufferedReader br = new BufferedReader(pointeur);
			String command = "";
			
			while(!(command = br.readLine()).equals("") && command != null){				
				System.out.println(command);
				twitters.executeUpdate(command);
			}
			
				System.out.println("Commande du fichier '"+file_name+"'... done\n");
			
			br.close(); 
		}		
		catch (Exception e){
			System.out.println("Erreur dans l'execution des commandes SQL : "+e.toString());
		}
	}
	
	/**
	 * Ecrit les donnees stockes dans un TweetManifold (structure de donnees du logiciel qui collecte les tweets de twitter) et les stocke dans la base de donnees.
	 * @param research
	 * @throws Exception
	 */
	public void write(TweetManifold research) throws Exception{
		ResultSet max = this.getfromtable(this.table()[0], "MAX(id)", "");
		
		Integer maxi;
		
		if (max.next()){
			maxi = max.getInt(1) + 1;
		}else{
			maxi = 1;
		}
		
		String request = research.getQueryString();
		
		if(request == null){
			request="null";
		}else{
			request = "'"+request+"'";
		}
		
		String field = maxi.toString()+ " , CURRENT_TIMESTAMP,"+request;
		this.addtotable(table()[0], field);
		
		ArrayList<Status> tweets = research.getCollection();
		
		for (int i = 0 ; i < tweets.size(); i++){
			Status current = tweets.get(i);
			User user = current.getUser();
			String name = null;
			String text = null;
			Timestamp sqldate; 
			
			if (user != null){
				name = user.getName();
				name = name.replace("'", " ");
			}
			
			if(name == null){
				name="null";
			}else{
				name = "'"+name+"'";
			}
			
			text = current.getText();
			
			if (text == null){
				text="null";
			}else{
				text = text.replace("'", " ");
				text = text.replace(":", " ");
			}
			
			sqldate = new Timestamp(current.getCreatedAt().getTime());
					
			field = current.getId()+" , "+maxi+","+name+", '"+sqldate+"', '"+text+ "'";
			this.addtotable(table()[1], field);
			HashtagEntity[] hashtags = current.getHashtagEntities();
			
			for(int j = 0; j < hashtags.length; j++){
				field = "'"+hashtags[j].getText()+" ', "+current.getId()+","+maxi;
				this.addtotable(table()[2], field);
			}
		}
		
		ArrayList<Looter> looters = research.getLooters();
		
		for(int i = 0; i < looters.size(); i++){
			write(looters.get(i), maxi);
		}
	}
	
	/*
	
	public void write(UserManifold manifold) throws SQLException{
		ResultSet max = this.getfromtable(this.table()[5], "MAX(id)", "");
		
		Integer maxi;
		
		if (max.next()){
			maxi = max.getInt(1) + 1;
		}else{
			maxi = 1;
		}
		
		String request = manifold.getCurrentUser();
		
		if(request == null){
			request="null";
		}else{
			request = "'"+request+"'";
		}
		
		String field = maxi.toString()+ " , CURRENT_TIMESTAMP,"+request;
		this.addtotable(table()[5], field);
		
		ArrayList<Looter> looters = manifold.getLooters();
		
		for(int i = 0; i < looters.size(); i++){
			write(looters.get(i), maxi);
		}
	}
	
	*/
	
	/**
	 * Cette fonction retourne la liste des ID des anciennes recherches, stockees dans la BD.
	 * Elle retourne avec ceux-ci les criteres de ces recherches.
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Couple<Integer,String>> getTweetSearch() throws SQLException{
		ArrayList<Couple<Integer,String>> result = new ArrayList<Couple<Integer,String>>();
		ResultSet hasht = this.getfromtable(this.table()[0], "*", "");
		
		while(hasht.next()){
			result.add(new Couple<Integer,String>(hasht.getInt(1),hasht.getString(3)));
		}
		
		return result;
	}
	
	/**
	 * Supprime des tables de la base de donnees toutes les informations associees à une recherche de tweet
	 * @param ID . Identifiant de la recherche de tweet.
	 * @throws SQLException 
	 */
	public void deleteTable (Integer ID) throws SQLException{
		String[] nametable = this.table();
		String command;
		
		for (int i = 2; i < 7 ; i++){
			command = "DELETE FROM "+nametable[i]+" WHERE research="+ID;
			try {
				this.twitters.executeUpdate(command);
				System.out.println(command);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//System.out.println("Requête SQL ");
			}
		}
		command = "DELETE FROM "+nametable[1]+" WHERE research="+ID;
		String command2 = "DELETE FROM "+nametable[0]+" WHERE id="+ID;
		try {
			this.twitters.executeUpdate(command);
			this.twitters.executeUpdate(command2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//System.out.println("Requête SQL ");
		}
		
	}
	
	/**
	 * Applique aux looters sa fonction, pour l'ecrire dans la bonne table.
	 * @param looter looter à ecrire dans la base de donnees.
	 * @param maxi id de la recherche qui a genere ce looter.
	 */
	private void write(Looter looter, Integer maxi) {
		// TODO Auto-generated method stub
		if (looter instanceof LanguageLooter){
			write((LanguageLooter) looter, maxi);
		}else if(looter instanceof LocationLooter){
			write((LocationLooter) looter, maxi);
		}else if(looter instanceof PopularityLooter){
			write((PopularityLooter) looter, maxi);
		}else if(looter instanceof HashtagLooter){
			write((HashtagLooter) looter, maxi);
		}else if (looter instanceof QuoteLooter){
			write((QuoteLooter) looter, maxi);
		}else if ((looter) instanceof RetweetLooter){
			write((RetweetLooter) looter, maxi);
		}else{
			System.out.println("Type de Looter inconnu");
		}
	}

	/**
	 * Ecrit dans la table associee, les informations d'un looter de type LanguageLooter
	 * @param looter. Looter a ecrire dans la base de donnees.
	 * @param ID. Identifiant de la recherche qui a genere ce looter.
	 */
	private void write(LanguageLooter looter, Integer ID){
		HashMap<String,Integer> data;
		int contain = 0;
		
		if ((data = looter.data()) == null){
			return;
		}

		
		Set<String> keys = data.keySet();
		for(String s : keys){
			contain = data.get(s);
			String field = "'"+s+"' , "+ID+" , " + contain ;
			this.addtotable(table()[3], field);
		}
		
		
	}
	
	/**
	 * Ecrit dans la table associee, les informations d'un looter de type HashtagLooter
	 * @param looter. Looter a ecrire dans la base de donnees.
	 * @param ID. Identifiant de la recherche qui a genere ce looter.
	 */
	private void write(HashtagLooter looter, Integer ID){
		HashMap<String,Integer> data;
		int contain = 0;
		
		if ((data = looter.data()) == null){
			return;
		}

		Set<String> keys = data.keySet();
		for(String s : keys){
			contain = data.get(s);
			String field = "'"+s+"' , "+ID+" , " + contain ;
			this.addtotable(table()[4], field);
		}
	}
	
	/**
	 * Ecrit dans la table associee, les informations d'un looter de type LocationLooter
	 * @param looter. Looter a ecrire dans la base de donnees.
	 * @param ID. Identifiant de la recherche qui a genere ce looter.
	 */
	private void write(LocationLooter looter, Integer ID){
		HashMap<GeoLocation,Integer> data = looter.data();
		Set<GeoLocation> keys =  data.keySet();
		int contain = 0;
		double longi = 0.0;
		double lati = 0.0;
		
		for (GeoLocation s : keys){
			contain = data.get(s);
			longi = s.getLongitude();
			lati = s.getLatitude();
			String field = longi+" , "+lati+" , '"+ID+"' , " + contain ;
			this.addtotable(table()[5], field);
		}
	}
	
	/**
	 * Ecrit dans la table associee, les informations d'un looter de type PopularityLooter
	 * @param looter. Looter a ecrire dans la base de donnees.
	 * @param ID. Identifiant de la recherche qui a genere ce looter.
	 */
	private void write(PopularityLooter looter, Integer ID){
		HashMap<Date,Integer> data;
		Timestamp sqlDate;
		int contain = 0;
		
		if ((data = looter.data()) == null){
			return;
		}

		Set<Date> keys = data.keySet();
		
		for(Date s : keys){
			contain = data.get(s);
			sqlDate = new Timestamp(s.getTime());
			String field = "'"+sqlDate+"' , "+ID+" , " + contain ;
			this.addtotable(table()[6], field);
		}
	}
	
	/*
	
	private void write(QuoteLooter looter, Integer ID){
		HashMap<String, Integer> data;
		int contain = 0;
		
		if ((data = looter.data()) == null){
			return;
		}

		Set<String> keys = data.keySet();
		for(String s : keys){
			contain = data.get(s);
			String field = "'"+s+"' , "+ID+" , " + contain ;
			this.addtotable(table()[6], field);
		}
	}
	
	private void write(RetweetLooter looter, Integer ID){
		HashMap<String, Integer> data;
		int contain = 0;
		
		if ((data = looter.data()) == null){
			return;
		}

		Set<String> keys = data.keySet();
		for(String s : keys){
			contain = data.get(s);
			String field = "'"+s+"' , "+ID+" , " + contain ;
			this.addtotable(table()[7], field);
		}
	}
	
	*/
	
	/**
	 * Genere un TweetManifold a partir des donnes extraite de la base de donnees. Cette structure de donnee peut etre ensuite geree par le logiciel pour generer les analyses.
	 * @param ID Identifiant de la recherche de tweet passee et dont on souhaite recuperer les informations.
	 * @return
	 */
	public TweetManifold extraire(Integer ID){
		ArrayList<Looter>looters = new ArrayList<Looter>();
		ArrayList<Tweet> addtweet = null;
		
		Looter looter;
		
		try {
			addtweet = extraire_tweet(ID);
			looter = extraire_language(ID);
			if (looter != null){
				looters.add(looter);
			}
			looter = extraire_hashtag(ID);
			if (looter != null){
				looters.add(looter);
			}
			looter = extraire_location(ID);
			if (looter != null){
				looters.add(looter);
			}
			looter = extraire_popularity(ID);
			if (looter != null){
				looters.add(looter);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Erreur dans la creation des looters d'extraction");
		}
		
		TweetManifold nouv = new TweetManifold(looters);
		
		nouv.setConversion(addtweet);
		
		return nouv;
	}
	
	/*
	
	public UserManifold extraire_user(Integer ID){
		return null;
	}
	
	*/
	
	private ArrayList<Tweet> extraire_tweet(Integer ID) throws SQLException{
		ResultSet select = this.getfromtable(this.table()[1], "*", "research = "+ID);
		ArrayList<Tweet> tweet = new ArrayList<Tweet>();
		
		
		while(select.next()){
			
			ArrayList<String> hash = new ArrayList<String>();
			
			/*
			String tweetid = select.getString(1);
			ResultSet select_hashtag = this.getfromtable(this.table()[2], "*", "tweetid ="+tweetid);
			while(select_hashtag.next()){
		
				
				String addhash = select_hashtag.getString(1);
				hash.add(addhash);
			}
			
			*/
			 
			Tweet addtweet = new Tweet(select.getString(3), select.getString(5), select.getTimestamp(4), hash);
			
			if (addtweet != null){
				tweet.add(addtweet);
			}
			
		}
		
		return tweet;
		
	}
	
	/**
	 * Extrait les informations de la table Language, et genere le looter associee.
	 * @param ID. Identifiant de la recherde de tweet passee, qui a genere ces informations.
	 * @return
	 * @throws SQLException
	 */
	private LanguageLooter extraire_language(Integer ID) throws SQLException{
		ResultSet select = this.getfromtable(this.table()[3], "*", "research = "+ID);
		HashMap<String,Integer> data = new HashMap<String, Integer>();
		String language;
		int count;
		int total = 0;
		
		while(select.next()){
			language = select.getString(1);
			count = select.getInt(3);
			data.put(language, count);
			total += count;
		}
		
		return new LanguageLooter(data, total);
		
		
	}
	
	/**
	 * Extrait les informations de la table Hastags, et genere le looter associee.
	 * @param ID. Identifiant de la recherde de tweet passee, qui a genere ces informations.
	 * @return
	 * @throws SQLException
	 */
	private HashtagLooter extraire_hashtag(Integer ID) throws SQLException{
		ResultSet select = this.getfromtable(this.table()[4], "*", "research = "+ID);
		HashMap<String,Integer> data = new HashMap<String, Integer>();
		String hashtag;
		int count;
		
		while(select.next()){
			hashtag = select.getString(1);
			count = select.getInt(3);
			data.put(hashtag, count);
		}
		
		
		return new HashtagLooter(data);
		
		
		
	}
	
	/**
	 * Extrait les informations de la table Location, et genere le looter associee.
	 * @param ID. Identifiant de la recherde de tweet passee, qui a genere ces informations.
	 * @return
	 * @throws SQLException
	 */
	private LocationLooter extraire_location(Integer ID) throws SQLException{
		ResultSet select = this.getfromtable(this.table()[5], "*", "research = "+ID);
		HashMap<GeoLocation, Integer> data = new HashMap<GeoLocation, Integer>();
		double longi;
		double lati;
		int count;
		GeoLocation geo;
		
		while(select.next()){
			longi= select.getDouble(1);
			lati=select.getDouble(2);
			geo = new GeoLocation(lati, longi);
			count = select.getInt(4);
			data.put(geo, count);
		}
		
		
		return new LocationLooter(data); 
		
		
		
	}
	
	/**
	 * Extrait les informations de la table Popularity, et genere le looter associee.
	 * @param ID. Identifiant de la recherde de tweet passee, qui a genere ces informations.
	 * @return
	 * @throws SQLException
	 */
	private PopularityLooter extraire_popularity(Integer ID) throws SQLException{
		ResultSet select = this.getfromtable(this.table()[6], "*", "research = "+ID);
		HashMap<Date, Integer> data = new HashMap<Date, Integer>();
		Date date;
		int count;
		
		while(select.next()){
			date = select.getTimestamp(1);
			count = select.getInt(3);
			System.out.println("date "+date+" "+count);
			data.put(date, count);
		}
		
		
			return new PopularityLooter(data);
		
	}
	
	/**
	private QuoteLooter extraire_quote(Integer ID) throws SQLException{
		ResultSet select = this.getfromtable(this.table()[6], "*", "research = "+ID);
		HashMap<String,Integer> data = new HashMap<String, Integer>();
		String user;
		int count;
		
		while(select.next()){
			user = select.getString(1);
			count = select.getInt(3);
			data.put(user, count);
		}
		return null;
		
	}
	
	private RetweetLooter extraire_retweet(Integer ID) throws SQLException{
		ResultSet select = this.getfromtable(this.table()[7], "*", "research = "+ID);
		HashMap<String,Integer> data = new HashMap<String, Integer>();
		String user;
		int count;
		
		while(select.next()){
			user = select.getString(1);
			count = select.getInt(3);
			data.put(user, count);
		}
		return null;
	}
	
	*/
	
	/**
	 * Affiche en console le contenu d'une table
	 * @param table. Table dont on souhaite observer le contenu.
	 * @throws SQLException
	 */
	public void tableaffiche(String table) throws SQLException{
		ResultSet wanted = this.getfromtable(table, "*", ""); 
		ResultSetMetaData wanted_data = wanted.getMetaData();
		int number_column = wanted_data.getColumnCount();
		
		System.out.println(table + "\n");
		
		while(wanted.next()){
			for (int i = 1; i < number_column; i++){
				System.out.print(wanted.getString(i) + " | ");
			}
				System.out.print(wanted.getString(number_column)+"\n");
				
		}
	}
	
	/**
	 * Ajoute a une table une entree passee en parametre
	 * @param table. Nom de la table
	 * @param field. Donnees que l'on souhaite entree dans la table, en format SQL. Les donnees sont triees par ordre de colonne et separer par des virgules.
	 */
	private void addtotable(String table, String field){
		String command = "INSERT INTO "+table+" VALUES ( " + field;
		
		command = command + " )";
		
		System.out.println(command);
		
		try{
			twitters.executeUpdate(command);
		}catch(Exception e){
			System.out.println(e);
		}
	}
	
	/**
	 * Retourne un ResultSet contenant les informations generees par la requete passees en parametre.
	 * @param table Table que sont on souhaite des informations.
	 * @param field Nom des colonnes dont on souhaite obtenir les informations au format SQL.Les colonnes apparaissent dans leur ordre dans la table, et sont séparés par des virgules.
	 * @param condition . Condition de recherche au format SQL.
	 * @return
	 * @throws SQLException
	 */
	private ResultSet getfromtable(String table, String field, String condition) throws SQLException{
		String command = "SELECT "+ field + " FROM " +table;
		
		if (condition != ""){
			command = command + " WHERE " + condition;
		}
		
		ResultSet request = twitters.executeQuery(command);
		
		return request;
		
	}
	
	/**
	 * Retourne un tableau contenant le nom des tables de la base de donnees.
	 * @return
	 */
	private String[] table(){
		
		String[] table ={"RESEARCHTWEET", "TWEET", "TWEETHASHTAGS","LANGUAGE", "HASHTAGS", "LOCATION", "TWEETDATE", "RESEARCHUSER", "QUOTE", "RETWEET"};
		return table;
	}
	
	public static void main(String[] args) throws Exception{
		DatabaseManager test = new DatabaseManager();
	
		TweetManifold test2 = new TweetManifold();
		
		test.command_file("CREATE"); 
		//test.write(test2);
		
		//test.tableaffiche(test.table()[0]);
		//test.extraire_popularity(2);
		//test.command_file("DROP");
		
		test.end();
		
	}
}
