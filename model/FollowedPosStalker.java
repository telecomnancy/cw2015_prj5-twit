package model;

import java.util.HashMap;
import twitter4j.*;

public class FollowedPosStalker extends Stalker{
	private HashMap<String, Integer> data;
	
	public FollowedPosStalker(){
		super();
		data=new HashMap<String, Integer>();
	}
	
	@Override
	public void take(User user) {
		String loc = user.getLocation();
		
		if(data.containsKey(loc)){
			int contain = data.get(loc) + 1;
			data.put(loc, contain);
		}else{
			data.put(loc, 1);
		}
		setChanged();
		notifyObservers();
		
	}

	@Override
	public HashMap<String, Integer> data() {
		return data;
	}

	@Override
	public String toString() {
		return "FollowedPosStalker";
	}

	public HashMap<GeoLocation, Integer> datatoGeolocation(){
		HashMap<GeoLocation, Integer> utile = new HashMap<GeoLocation, Integer>();
		
		String [] keys = (String[]) data.keySet().toArray();
		GeoQuery geo = new GeoQuery((String)null);
		
		for (int i =0; i < keys.length; i++){
			geo.setQuery(keys[i]);
			utile.put(geo.getLocation(), data.get(keys[i]));
		}
		
		return utile;
		
		
	}
}
