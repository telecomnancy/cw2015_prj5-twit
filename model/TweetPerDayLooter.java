package model;

import java.util.*;
import twitter4j.Status;

public class TweetPerDayLooter extends Looter {
	
	private HashMap<Date,Integer> data;
	
	public TweetPerDayLooter() {
		this.data = new HashMap<Date,Integer>();
	}
	
	public TweetPerDayLooter(HashMap<Date,Integer> data) {
		this.data = data;
	}
	
	@Override
	public void take(Status status) {
		Date date = status.getCreatedAt();
		// On met la date a 0:00 du jour courant (pour regrouper les tweets par jour)
		date.setTime((date.getTime()-(date.getTime()%(1000*60*60*24))));
		if (data.containsKey(date)) {
			this.data.put(date, data.get(date)+1);
		} else {
			this.data.put(date, 1);
		}
	}

	@Override
	public String toString() {
		return "TweetPerDayLooter";
	}

	@Override
	public HashMap<Date,Integer> data() {
		return this.data;
	}

	@Override
	public void clear() {
		this.data=new HashMap<Date,Integer>();
	}

}
