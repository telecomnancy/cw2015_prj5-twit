package model;

import java.util.*;
import twitter4j.*;

public class Tweet {
	private String author, text;
	private Date date;
	private ArrayList<String> hashtags;
	
	public Tweet(String auth, String txt, Date d, ArrayList<String> hash){
		author = auth;
		text = txt;
		date = d;
		hashtags = hash;
	}
	
	public Tweet(String auth, String txt, Date d){
		author = auth;
		text = txt;
		date = d;
		hashtags = new ArrayList<>();
	}
	
	public Tweet(Status s){
		author = s.getUser().getScreenName();
		text = s.getText();
		date = s.getCreatedAt();
		hashtags = new ArrayList<String>();
		HashtagEntity[] tab = s.getHashtagEntities();
		
		for (int i = 0; i < tab.length; i++) {
			hashtags.add(tab[i].getText());
		}
	}
	
	public void addHashtag(String hash){
		hashtags.add(hash);
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ArrayList<String> getHashtags() {
		return hashtags;
	}
}
