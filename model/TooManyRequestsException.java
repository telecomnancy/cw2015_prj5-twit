package model;

public class TooManyRequestsException extends Exception{
	public TooManyRequestsException(){
		super("Sorry, but you have send too many request, and now Twitter is angry ! Try again later ;)");
	}
}
