package model;

import java.util.HashMap;

import twitter4j.*;

public class QuoteLooter extends Looter {
	
	private HashMap<String, Integer> data;
	private Twitter t = TwitterFactory.getSingleton();
	
	public QuoteLooter() {
		super();
		data = new HashMap<String, Integer>();
	}
	
	public QuoteLooter(HashMap<String, Integer> data) {
		super();
		this.data=data;
	}
	@Override
	public void take(Status status) {
		UserMentionEntity[] mentioned = status.getUserMentionEntities();
		if (mentioned.length==0) return;
		
		// JE recupere les noms des users mentiones
		String[] names=new String[mentioned.length];
		for(int i=0;i<mentioned.length;i++) {
			names[i]=mentioned[i].getScreenName();
		}
		
		// Je les enregistre et/ou incremente
		for(int i=0;i<names.length;i++) {
			if(data.containsKey(names[i])) {
				data.put(names[i], data.get(names[i])+1);
			} else {
				data.put(names[i], 1);
			}
		}
		setChanged();
		notifyObservers();
	}

	@Override
	public HashMap<String, Integer> data() {
		return this.data;
	}

	@Override
	public void clear() {
		data = new HashMap<String,Integer>();
	}
	@Override
	public String toString() {
		return "QuoteLooter";
	}

}
