package model;

/**
 * Exceptions lev�es si une query ne retourne aucun r�sultat
 *
 */
public class NoResultException extends Exception{
	public NoResultException(){
		super("Aucun tweet n'a �t� trouv� !");
	}
}
