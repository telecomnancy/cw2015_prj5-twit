package model;

import java.util.HashMap;
import twitter4j.*;

/**
 * Annalyse et stocke la correlation entre un hashtag et d'autres hastags
 * 
 */
public class HashtagLooter extends Looter{
	/**
	 * Sert a stocker les donn�es g�n�r�es par HashtagLooter
	 * String : correspond � un hashtag
	 * Integer : nombre de fois qu'un hashtag est pr�sent
	 */
	private HashMap<String, Integer> data;
	
	/**
	 * Constructeur de HashtagLooter
	 */
	public HashtagLooter(){
		super();
		data=new HashMap<String,Integer>();
	}
	
	/**
	 * Setter de data
	 * @param newdata HashMap<String, Integer>
	 */
	public HashtagLooter(HashMap<String, Integer> newdata){
		this.data = newdata;
	}
	
	/**
	 * Incr�mente ou ajoute des hashtags � data � partir d'un Status (si hashtag pr�sent +1)
	 */
	@Override
	public void take(Status status) {
		synchronized (HashtagLooter.class) {
			HashtagEntity[] hash = status.getHashtagEntities();
			String hash_text;
			
			if(hash.length == 0) return;
			
			for (int i=0; i < hash.length; i++){
				
				hash_text = hash[i].getText();
				hash_text = hash_text.toLowerCase();
				
				if (data.containsKey(hash_text)){
					//contain = data.get(hash_text) + 1;
					data.put(hash_text, data.get(hash_text)+1);
				}else{
					data.put(hash_text, 1);
				}
			}
			
			setChanged();
			notifyObservers();
		}
	}
	
	@Override
	public String toString() {
		return "CorrelationLooter";
	}
	
	/**
	 * Getter de data
	 */
	@Override
	public HashMap<String, Integer> data() {
		return this.data;
	}
	
	
	/**
	 * Remise � zero de data
	 */
	@Override
	public void clear() {
		data = new HashMap<String, Integer>();
	}

}
